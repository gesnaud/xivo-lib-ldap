package xivo.ldap;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import xivo.ldap.configuration.ConfigLoader;
import xivo.ldap.configuration.Configuration;
import xivo.ldap.configuration.ExitCodes;
import xivo.ldap.ldapconnection.FieldMapping;
import xivo.restapi.model.User;

public abstract class AbstractDeltaGenerator {

    protected List<User> initializedUsers = new LinkedList<User>();
    protected List<User> usersToUpdate = new ArrayList<User>();
    protected List<User> usersToCreate = new ArrayList<User>();
    protected List<User> updatesToLdap = new ArrayList<User>();
    protected List<User> usersToDelete = new ArrayList<User>();
    protected Set<String> fieldsToCopy;
    protected Logger logger = Logger.getLogger(getClass().getName());

    public AbstractDeltaGenerator() {
        Configuration config = ConfigLoader.getConfig();
        FieldMapping fm = config.getFieldMapping();
        fieldsToCopy = fm.getAllXivoFields();
        fieldsToCopy.removeAll(config.getMasteredFields());
    }

    protected abstract boolean xivoNeedsUpdate(User xivoUser, User ldapUser);

    protected abstract boolean ldapNeedsUpdate(User xivoUser, User ldapUser);

    public void generateDelta(List<User> xivoUsers, List<User> ldapUsers) {
        extractInitializedUsers(xivoUsers);
        while (!ldapUsers.isEmpty()) {
            User ldapUser = ldapUsers.remove(0);
            User xivoUser = findMatchingUser(ldapUser, initializedUsers);
            if (xivoUser != null) {
                logger.finest("Found xivo user: " + xivoUser + " for ldap user: " + ldapUser);
                initializedUsers.remove(xivoUser);
                if (xivoNeedsUpdate(xivoUser, ldapUser)) {
                    try {
                        copyFieldsFromLdapToXivo(ldapUser, xivoUser);
                    } catch (IllegalAccessException e) {
                        logger.log(Level.SEVERE, "Error accessing a user field, exiting.", e);
                        System.exit(ExitCodes.UNREADABLE_USERFIELD.ordinal());
                    } catch (NoSuchFieldException e) {
                        logger.log(Level.SEVERE, "Field mapping not properly configured, exiting.", e);
                        System.exit(ExitCodes.CONFIGURATION_ERROR.ordinal());
                    }
                    usersToUpdate.add(xivoUser);
                }
                if (ldapNeedsUpdate(xivoUser, ldapUser)) {
                    updatesToLdap.add(xivoUser);
                }
            } else if (needsCreation(ldapUser)) {
                logger.finest("Xivo user not found for ldap user: " + ldapUser);
                usersToCreate.add(ldapUser);
            }
        }
        for (User user : initializedUsers) {
            if(needsDeletion(user)) {
                logger.finest("User to delete: " + user);
                usersToDelete.add(user);
            }
        }
    }

    protected void copyFieldsFromLdapToXivo(User ldapUser, User xivoUser) throws IllegalAccessException,
            NoSuchFieldException {
        for (String field : fieldsToCopy) {
            Object value = UserIntrospector.getUserField(ldapUser, field);
            if (value != null)
                UserIntrospector.setUserField(xivoUser, field, value.toString());
            else
                UserIntrospector.setUserField(xivoUser, field, null);
        }
        if(ldapUser.getLine() == null && ldapMasters("Line"))
            xivoUser.setLine(null);
        if(ldapUser.getVoicemail() == null && ldapMasters("Voicemail"))
            xivoUser.setVoicemail(null);
        if(ldapUser.getIncomingCall() == null && ldapMasters("IncomingCall"))
            xivoUser.setIncomingCall(null);
    }

    private boolean ldapMasters(String object) {
        for(String prop: fieldsToCopy) {
            if(prop.startsWith(object)) return true;
        }
        return false;
    }

    protected void extractInitializedUsers(List<User> users) {
        for (User user : users) {
            if (user.getDescription() != null && !user.getDescription().equals("")) {
                initializedUsers.add(user);
            }
        }
    }

    protected boolean needsCreation(User ldapUser) {
        return true;
    }

    protected boolean needsDeletion(User xivoUser) {
        return true;
    }

    protected User findMatchingUser(User user, List<User> users) {
        String description = user.getDescription();
        if (description == null) {
            logger.warning("User with null description : " + user);
            return null;
        }
        for (User otherUser : users) {
            if (otherUser.getDescription() == null) {
                logger.warning("User with null description : " + otherUser);
                continue;
            }
            if (description.equals(otherUser.getDescription())) {
                return otherUser;
            }
        }
        return null;
    }

    public List<User> getUsersToUpdate() {
        return usersToUpdate;
    }

    public List<User> getUsersToCreate() {
        return usersToCreate;
    }

    public List<User> getUsersToDelete() {
        return usersToDelete;
    }

    public List<User> getUpdatesToLdap() {
        return updatesToLdap;
    }

}
