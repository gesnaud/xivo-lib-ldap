package xivo.ldap;

import java.util.List;

import xivo.restapi.model.User;

public interface Initializer {

    public void initialize(List<User> xivoUsers, List<User> ldapUsers);

    public List<User> getUsersToUpdate();

}
