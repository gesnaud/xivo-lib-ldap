package xivo.ldap.configuration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;

import xivo.ldap.ldapconnection.FieldMapping;

public class ConfigLoader {

    private static final String FIELD_MAPPING_SECTION = "FieldMapping";
    private static final String LDAP_SECTION = "LDAP";
    private static final String XIVO_SECTION = "XiVO";
    private static final String ACTIONS_SECTION = "Actions";
    private static final String GENERAL_SECTION = "General";
    private static final String RESTAPI_SECTION = "Restapi";
    private static final String RESTAPI_FILE = "config/general.ini";
    private static final String CONFIG_FILE = "config/config.ini";

    private Logger logger = Logger.getLogger(getClass().getName());
    private FieldMapping fieldMapping;
    private Actions users;
    private Actions voicemails;
    private Actions lines;
    private Actions incalls;
    private Set<String> updatedLdapFields;
    private Map<String, String> params;

    private static Configuration configuration;
    private static Logger staticLogger = Logger.getLogger(ConfigLoader.class.getName());

    private Configuration loadConfig() throws IOException {
        logger.info("Loading configuration:");
        params = new HashMap<String, String>();
        loadRestapiConfig();
        loadConnectorConfig();
        Configuration config = new Configuration();
        config.params = params;
        config.fieldMapping = fieldMapping;
        config.incalls = incalls;
        config.lines = lines;
        config.users = users;
        config.voicemails = voicemails;
        config.updatedLdapFields = updatedLdapFields;
        logger.info("* General parameters    : " + params);
        logger.info("* Field mapping         : " + fieldMapping);
        logger.info("* Actions sur incalls   : " + incalls);
        logger.info("* Actions sur lines     : " + lines);
        logger.info("* Actions sur users     : " + users);
        logger.info("* Actions sur voicemails: " + voicemails);
        logger.info("* updatedLdapFields     : " + updatedLdapFields);
        return config;
    }

    private void loadConnectorConfig() throws InvalidFileFormatException, IOException {
        Ini ini = new Ini(new File(CONFIG_FILE));
        loadStandardSections(ini, XIVO_SECTION, LDAP_SECTION);
        loadActionsSection(ini);
        loadFieldMappingSection(ini);
    }

    private void loadStandardSections(Ini ini, String... sectionNames) {
        for (String sectionName : sectionNames) {
            if (ini.containsKey(sectionName)) {
                Ini.Section section = ini.get(sectionName);
                for (String key : section.keySet()) {
                    params.put(key, section.get(key));
                }
            }
        }
        if (params.containsKey("reportFolder")) {
            String reportFolder = params.get("reportFolder");
            if (!reportFolder.endsWith("/")) {
                reportFolder += "/";
                params.put("reportFolder", reportFolder);
            }
        }
    }

    private void loadRestapiConfig() throws InvalidFileFormatException, IOException {
        Ini ini = new Ini(new File(RESTAPI_FILE));
        loadStandardSections(ini, GENERAL_SECTION, RESTAPI_SECTION);
    }

    private void loadActionsSection(Ini ini) {
        if (ini.containsKey(ACTIONS_SECTION)) {
            Ini.Section actionsSection = ini.get(ACTIONS_SECTION);
            users = createActions(actionsSection.get("User", ""));
            voicemails = createActions(actionsSection.get("Voicemail", ""));
            lines = createActions(actionsSection.get("Line", ""));
            incalls = createActions(actionsSection.get("IncomingCall", ""));
            String fieldsString = actionsSection.get("updatedLdapFields");
            if (fieldsString == null)
                updatedLdapFields = new HashSet<String>();
            else
                updatedLdapFields = new HashSet<String>(Arrays.asList(fieldsString.split(",")));
        }
    }

    private Actions createActions(String actions) {
        List<String> actionsList = Arrays.asList(actions.split(","));
        boolean create = actionsList.contains("C");
        boolean update = actionsList.contains("U");
        boolean delete = actionsList.contains("D");
        return new Actions(create, update, delete);
    }

    private void loadFieldMappingSection(Ini ini) {
        if (ini.containsKey(FIELD_MAPPING_SECTION)) {
            Ini.Section fieldMappingSection = ini.get(ConfigLoader.FIELD_MAPPING_SECTION);
            fieldMapping = new FieldMapping();
            for (String xivoField : fieldMappingSection.keySet()) {
                fieldMapping.addFieldMapping(fieldMappingSection.get(xivoField), xivoField);
            }
        }
    }

    public static Configuration getConfig() {
        if (configuration == null) {
            ConfigLoader configLoader = new ConfigLoader();
            try {
                configuration = configLoader.loadConfig();
            } catch (InvalidFileFormatException e) {
                staticLogger.log(Level.SEVERE, "Configuration file not found, exiting.", e);
                System.exit(ExitCodes.CONFIGURATION_ERROR.ordinal());
            } catch (IOException e) {
                staticLogger.log(Level.SEVERE, "Could not parse the configuration file, exiting.", e);
                System.exit(ExitCodes.CONFIGURATION_ERROR.ordinal());
            }
        }

        return configuration;
    }
}
