package xivo.ldap.configuration;

public class Actions {

    public boolean create;
    public boolean update;
    public boolean delete;

    public Actions(boolean create, boolean update, boolean delete) {
        this.create = create;
        this.update = update;
        this.delete = delete;
    }

    public String toString() {
    	return "Create: " + create + ", update: " + update + ", delete: " + delete;
    }
}
