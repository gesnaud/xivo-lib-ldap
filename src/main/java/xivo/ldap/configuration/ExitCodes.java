package xivo.ldap.configuration;

public enum ExitCodes {
    LDAP_ACCESS_ERROR(1),
    CONFIGURATION_ERROR(2),
    UNREADABLE_USERFIELD(3),
    LOG_CONFIGURATION_ERROR(4),
    WRONG_CLI_PARAMETERS(5),
    REST_API_ERROR(6),
    CODE_STRUCTURE_ERROR(7);

    private int value;

    private ExitCodes(int value) {
        this.value = value;
    }

}
