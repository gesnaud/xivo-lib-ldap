package xivo.ldap;

import org.apache.commons.cli.CommandLine;

public class ParsingResult {

    private CommandLine command;

    public ParsingResult(CommandLine command) {
        this.command = command;
    }

    public boolean isInitiatialization() {
        return command.hasOption(ArgumentParser.INITIATE);
    }

    public boolean isDryRun() {
        return command.hasOption(ArgumentParser.DRYRUN);
    }

}
