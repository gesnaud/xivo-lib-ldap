package xivo.ldap;

import java.lang.reflect.Field;

import xivo.restapi.model.IncomingCall;
import xivo.restapi.model.Line;
import xivo.restapi.model.User;
import xivo.restapi.model.Voicemail;

public class UserIntrospector {
    public static Object getUserField(User user, String field) throws IllegalArgumentException, IllegalAccessException,
            NoSuchFieldException {
        String[] fieldDecomposition = decomposeField(field);
        String clazz = fieldDecomposition[0];
        String fieldName = fieldDecomposition[1];
        Object object;
        try {
            object = getObjectFromUser(user, clazz);
        } catch (ClassNotFoundException e) {
            throw new NoSuchFieldException(fieldName);
        }
        if (object == null) {
            return null;
        }
        return getAttribute(object, fieldName);
    }

    public static void setUserField(User user, String field, String value) throws IllegalArgumentException,
            IllegalAccessException, NoSuchFieldException {
        String[] fieldDecomposition = decomposeField(field);
        String clazz = fieldDecomposition[0];
        String fieldName = fieldDecomposition[1];
        Object object;
        try {
            object = getOrCreateObjectWithUser(user, clazz);
        } catch (ClassNotFoundException e) {
            throw new NoSuchFieldException(field);
        }
        setAttribute(object, fieldName, value);
    }

    private static Object getAttribute(Object object, String fieldName) throws IllegalArgumentException,
            SecurityException, IllegalAccessException, NoSuchFieldException {
        Field field = getFieldFromName(object, fieldName);
        field.setAccessible(true);
        return field.get(object);
    }

    private static Field getFieldFromName(Object object, String fieldName) throws NoSuchFieldException {
        Class clazz = object.getClass();
        while (clazz.getSuperclass() != null) {
            try {
                return clazz.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
        }
        throw new NoSuchFieldException(fieldName);
    }

    private static String[] decomposeField(String field) throws NoSuchFieldException {
        String[] fieldDecomposition = field.split("\\.");
        if (fieldDecomposition.length != 2) {
            throw new NoSuchFieldException(field);
        }
        return fieldDecomposition;
    }

    private static Object getObjectFromUser(User user, String objectClass) throws ClassNotFoundException {
        if (objectClass.equals("User")) {
            return user;
        } else if (objectClass.equals("Line")) {
            return user.getLine();
        } else if (objectClass.equals("Voicemail")) {
            return user.getVoicemail();
        } else if (objectClass.equals("IncomingCall")) {
            return user.getIncomingCall();
        } else {
            throw new ClassNotFoundException();
        }
    }

    private static Object createEmptyObject(String objectClass) throws ClassNotFoundException {
        if (objectClass.equals("Line")) {
            return new Line();
        } else if (objectClass.equals("Voicemail")) {
            return new Voicemail();
        } else if (objectClass.equals("IncomingCall")) {
            return new IncomingCall();
        } else {
            throw new ClassNotFoundException();
        }
    }

    private static void setCreatedToUser(User user, Object objectToSet) throws ClassNotFoundException {
        if (objectToSet.getClass() == Line.class) {
            user.setLine((Line) objectToSet);
        } else if (objectToSet.getClass() == Voicemail.class) {
            user.setVoicemail((Voicemail) objectToSet);
        } else if (objectToSet.getClass() == IncomingCall.class) {
            user.setIncomingCall((IncomingCall) objectToSet);
        } else {
            throw new ClassNotFoundException();
        }
    }

    private static Object getOrCreateObjectWithUser(User user, String objectClass) throws ClassNotFoundException {
        Object created = getObjectFromUser(user, objectClass);
        if (created == null) {
            created = createEmptyObject(objectClass);
            setCreatedToUser(user, created);
        }
        return created;
    }

    private static void setAttribute(Object object, String fieldName, String value) throws IllegalArgumentException,
            SecurityException, IllegalAccessException, NoSuchFieldException {
        Field field = getFieldFromName(object, fieldName);
        field.setAccessible(true);
        if (field.getGenericType() == Integer.class || field.getGenericType() == int.class) {
            field.set(object, Integer.parseInt(value));
        } else {
            field.set(object, value);
        }
    }
}
