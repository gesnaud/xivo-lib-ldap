package xivo.ldap.xivoconnection;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import xivo.restapi.model.Context;
import xivo.restapi.model.ContextInterval;

import com.google.inject.Inject;
import com.google.inject.name.Named;

public class NumberGenerator {

    protected List<String> existingNumbers;
    protected List<String> generatedNumbers = new LinkedList<String>();
    protected Context context;

    public NumberGenerator(Context context, List<String> existingNumbers) {
        this.existingNumbers = existingNumbers;
        this.context = context;
    }

    public String generateLineNumber() {
        String res = null;
        for (ContextInterval it : context.getIntervals()) {
            res = generateNumberInInterval(it);
            if (res != null)
                break;
        }
        return res;
    }

    private String generateNumberInInterval(ContextInterval it) {
        String realStart = it.start.substring(it.prefix.length());
        String realEnd = it.end.substring(it.prefix.length());
        for (int i = Integer.parseInt(realStart); i <= Integer.parseInt(realEnd); i++) {
            String res = paddWithZeros(i, realStart.length());
            if (!(existingNumbers.contains(res) || generatedNumbers.contains(res))) {
                generatedNumbers.add(res);
                return it.prefix + res;
            }
        }
        return null;
    }

    public void addExistingNumber(String n) {
        existingNumbers.add(n);
    }

    public boolean isNumberUsed(String number) {
        return existingNumbers.contains(number) || generatedNumbers.contains(number);
    }

    private String paddWithZeros(int i, int length) {
        int numLength = Integer.toString(i).length();
        int diff = length - numLength;
        String res = "";
        for(int j=0; j<diff; j++) {
            res += '0';
        }
        return res + i;
    }

    public ContextInterval getIntervalForNumber(String number) {
        return context.getIntervalForNumber(number);
    }
}
