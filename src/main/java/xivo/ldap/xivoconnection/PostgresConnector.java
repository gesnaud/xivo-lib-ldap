package xivo.ldap.xivoconnection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import xivo.restapi.model.Context;
import xivo.restapi.model.ContextInterval;
import xivo.restapi.model.IncomingCall;
import xivo.restapi.model.Trunk;
import xivo.restapi.model.User;

import com.google.inject.Inject;

public class PostgresConnector {

    private PostgresConnectionFactory connFactory;
    private Logger logger = Logger.getLogger(getClass().getName());

    @Inject
    public PostgresConnector(PostgresConnectionFactory connFactory) {
        this.connFactory = connFactory;
    }

    public IncomingCall getIncomingCallForUser(User user) throws IOException {
        String sda = null;
        try {
            Statement statement = connFactory.get().createStatement();
            String query = "SELECT i.exten FROM incall i INNER JOIN dialaction d ON CAST(i.id AS VARCHAR) = d.categoryval"
                    + " WHERE d.event = 'answer' AND d.action = 'user' AND d.category = 'incall' AND d.actionarg1 = '"
                    + user.getId() + "'";
            ResultSet result = statement.executeQuery(query);
            if (result.next()) {
                sda = result.getString("exten");
                logger.fine("user of id " + user.getId() + " has sda " + sda);
            }
            result.close();
            statement.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Got an SQL exception while getting the SDAs", e);
        }
        if (sda != null)
            return new IncomingCall(sda);
        return null;
    }

    public List<String> getFreeSdas() {
        List<String> resultList = new LinkedList<String>();
        try {
            Statement statement = connFactory.get().createStatement();
            String query = "SELECT i.exten FROM incall i INNER JOIN dialaction d ON CAST(i.id AS VARCHAR) = d.categoryval "
                    + "AND category = 'incall' AND action = 'none'";
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                resultList.add(result.getString("exten"));
            }
            result.close();
            statement.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Got an SQL exception while getting unused SDAs", e);
        }
        return resultList;
    }

    public void associateIncallToUser(IncomingCall incall, int userId) throws SQLException {
        Connection connection = connFactory.get();
        try {
            int incallId = getIncallId(connection, incall);
            if (incallId > 0) {
                connection.setAutoCommit(false);
                dissociateOldIncall(connection, userId);
                associateIncall(connection, userId, incallId);
                connection.commit();
            }
        } catch (SQLException e) {
            connection.rollback();
            logger.log(Level.SEVERE, "Error when associating a SDA to a user", e);
        } finally {
            connection.setAutoCommit(true);
        }

    }

    private void associateIncall(Connection connection, int userId, int incallId) throws SQLException {
        String queryAssociateIncall = "UPDATE dialaction SET action = 'user', actionarg1 = ?, linked = 1 WHERE category = 'incall' and categoryval= ?";
        PreparedStatement statementAssociateIncall = connection.prepareStatement(queryAssociateIncall);
        statementAssociateIncall.setString(1, Integer.toString(userId));
        statementAssociateIncall.setString(2, Integer.toString(incallId));
        statementAssociateIncall.executeUpdate();
        statementAssociateIncall.close();
    }

    private void dissociateOldIncall(Connection connection, int userId) throws SQLException {
        String queryDissociateOldIncall = "UPDATE dialaction SET action = ?, actionarg1 = NULL WHERE category = ? AND action = ? AND actionarg1 = ?";
        PreparedStatement statementDissociateOldIncall = connection.prepareStatement(queryDissociateOldIncall);
        statementDissociateOldIncall.setString(1, "none");
        statementDissociateOldIncall.setString(2, "incall");
        statementDissociateOldIncall.setString(3, "user");
        statementDissociateOldIncall.setString(4, Integer.toString(userId));
        statementDissociateOldIncall.executeUpdate();
        statementDissociateOldIncall.close();
    }

    private int getIncallId(Connection connection, IncomingCall incall) throws SQLException {
        String query = "SELECT id FROM incall WHERE exten = ? AND context = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, incall.getSda());
        statement.setString(2, incall.getContext());

        ResultSet result = statement.executeQuery();
        int returnValue = 0;
        if (result.next()) {
            returnValue = result.getInt("id");
        }
        result.close();
        statement.close();
        return returnValue;
    }

    public Context getContext(String contextName, String type) throws EmptyContextException {
        String query = "SELECT numberbeg, numberend, didlength FROM contextnumbers WHERE context = ? AND type = ?";
        Context result = new Context(contextName, new LinkedList<ContextInterval>());
        try {
            PreparedStatement statement = connFactory.get().prepareStatement(query);
            statement.setString(1, contextName);
            statement.setString(2, type);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String start = resultSet.getString("numberbeg");
                String end = resultSet.getString("numberend");
                int didLength = resultSet.getInt("didlength");
                result.addInterval(new ContextInterval(start, end, didLength));
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error getting the list of numbers for context " + contextName, e);
        }
        if (result.getIntervals().size() == 0)
            throw new EmptyContextException();
        else
            return result;
    }

    public boolean incomingCallExists(IncomingCall incall) throws SQLException {
        String query = "SELECT * FROM incall WHERE exten = ? AND context = ?";
        PreparedStatement statement = connFactory.get().prepareStatement(query);
        statement.setString(1, incall.getSda());
        statement.setString(2, incall.getContext());
        ResultSet rows = statement.executeQuery();
        boolean result = rows.next();
        rows.close();
        statement.close();
        return result;
    }

    public void createIncall(IncomingCall incall) throws SQLException {
        Connection connection = connFactory.get();
        try {
            connection.setAutoCommit(false);
            int incallId = createIncallRow(connection, incall);
            createExtensionsRow(connection, incall, incallId);
            createIncallDialaction(connection, incallId);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            logger.log(Level.SEVERE, "Error creating the incoming call " + incall.getSda(), e);
        } finally {
            connection.setAutoCommit(true);
        }
    }

    private void createIncallDialaction(Connection connection, int id) throws SQLException {
        String query = "INSERT INTO dialaction (event, category, categoryval, action, linked) VALUES ('answer', 'incall', ?, 'none', 1)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Integer.toString(id));
        statement.executeUpdate();
    }

    private void createExtensionsRow(Connection connection, IncomingCall incall, int incallId) throws SQLException {
        String query = "INSERT INTO extensions (context, exten, type, typeval) VALUES ('from-extern', ?, 'incall', ?)";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, incall.getSda());
        statement.setString(2, Integer.toString(incallId));
        statement.executeUpdate();
    }

    private int createIncallRow(Connection connection, IncomingCall incall) throws SQLException {
        String query = "INSERT INTO incall(exten, context, description) VALUES (?, 'from-extern', '')";
        PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, incall.getSda());
        statement.executeUpdate();
        ResultSet generatedKeys = statement.getGeneratedKeys();
        if (generatedKeys.next()) {
            int result = generatedKeys.getInt(1);
            generatedKeys.close();
            return result;
        } else {
            generatedKeys.close();
            throw new SQLException("No id generated for incall " + incall.getSda());
        }
    }

    public void deleteIncall(IncomingCall incall) throws SQLException {
        Connection connection = connFactory.get();
        try {
            connection.setAutoCommit(false);
            int incallId = getIncallId(connection, incall);
            deleteIncallRow(connection, incallId);
            deleteExtensionRow(connection, incall);
            deleteIncallDialactions(connection, Integer.toString(incallId));
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            logger.log(Level.SEVERE, "Could not delete the incoming call " + incall.getSda(), e);
        } finally {
            connection.setAutoCommit(true);
        }
    }

    private void deleteIncallDialactions(Connection connection, String incallId) throws SQLException {
        String query = "DELETE FROM dialaction WHERE category = 'incall' AND categoryval = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, incallId);
        statement.executeUpdate();
    }

    private void deleteExtensionRow(Connection connection, IncomingCall incall) throws SQLException {
        String query = "DELETE FROM extensions WHERE exten = ? AND context = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, incall.getSda());
        statement.setString(2, incall.getContext());
        statement.executeUpdate();
    }

    private void deleteIncallRow(Connection connection, int id) throws SQLException {
        String query = "DELETE FROM incall WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        statement.executeUpdate();
    }

    public void createContext(Context context) throws SQLException {
        Connection connection = connFactory.get();
        try {
            connection.setAutoCommit(false);
            createContextOnly(connection, context);
            createContextNumbers(connection, context);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            logger.log(Level.SEVERE, "Could not create the conetxt " + context.getName(), e);
            throw e;
        } finally {
            connection.setAutoCommit(true);
        }
    }

    private void createContextNumbers(Connection connection, Context context) throws SQLException {
        for (ContextInterval ci : context.getIntervals()) {
            String query = "INSERT INTO contextnumbers(context, type, numberbeg, numberend) VALUES (?, 'user', ?, ?)";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, context.getName());
            statement.setString(2, ci.start);
            statement.setString(3, ci.end);
            statement.executeUpdate();
        }

    }

    private void createContextOnly(Connection connection, Context context) throws SQLException {
        String entity = getFirstEntity(connection);
        String query = "INSERT INTO context(name, displayname, entity, contexttype, description) VALUES (?, ?, ?, ?, '')";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, context.getName());
        statement.setString(2, context.getName());
        statement.setString(3, entity);
        statement.setString(4, context.getType().toString());
        statement.executeUpdate();
    }

    private String getFirstEntity(Connection connection) throws SQLException {
        String query = "SELECT name FROM entity";
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet rs = statement.executeQuery();
        String entity = null;
        if (rs.next())
            entity = rs.getString("name");
        rs.close();
        return entity;
    }

    public void createContextInclusion(String context, String includedContext, int priority) throws SQLException {
        String query = "INSERT INTO contextinclude(context, include, priority) VALUES (?, ?, ?)";
        PreparedStatement statement = connFactory.get().prepareStatement(query);
        statement.setString(1, context);
        statement.setString(2, includedContext);
        statement.setInt(3, priority);
        statement.executeUpdate();
    }

    public void createTrunk(Trunk trunk) throws SQLException {
        Connection connection = connFactory.get();
        try {
            connection.setAutoCommit(false);
            int sipId = createUserSip(connection, trunk);
            String query = "INSERT INTO trunkfeatures(protocol, protocolid) VALUES ('sip', ?)";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, sipId);
            statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            logger.log(Level.SEVERE, "Could not create the trunk " + trunk.getName(), e);
            throw e;
        } finally {
            connection.setAutoCommit(true);
        }
    }

    private int createUserSip(Connection connection, Trunk trunk) throws SQLException {
        String query = "INSERT INTO usersip(name, type, username, context, host, protocol, category, transport) VALUES "
                + "(?, 'friend', ?, ?, ?, 'sip', 'trunk', 'udp')";
        PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, trunk.getName());
        statement.setString(2, trunk.getName());
        statement.setString(3, trunk.getContext());
        statement.setString(4, trunk.getDestinationIp());
        statement.executeUpdate();
        ResultSet generatedKeys = statement.getGeneratedKeys();
        if (generatedKeys.next()) {
            int result = generatedKeys.getInt(1);
            generatedKeys.close();
            return result;
        } else {
            generatedKeys.close();
            throw new SQLException("No id generated for trunk " + trunk.getName());
        }
    }

    public boolean extensionExists(String exten, String context) throws SQLException {
        Connection connection = connFactory.get();
        String query = "SELECT id FROM extensions WHERE exten = ? AND context = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, exten);
        statement.setString(2, context);
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            rs.close();
            return true;
        } else {
            rs.close();
            return false;
        }
    }

    public void createWebServicesUser(String login, String password, String address) throws SQLException {
        String query = "INSERT INTO accesswebservice(name, login, passwd, host, description, obj) VALUES (?, ?, ?, ?, '', '')";
        Connection c = connFactory.get();
        PreparedStatement st = c.prepareStatement(query);
        st.setString(1, login);
        st.setString(2, login);
        st.setString(3, password);
        st.setString(4, address);
        st.executeUpdate();
        st.close();
    }

    // to be used for xivo version 16.01 and higher
    public void createWebServicesUserWithAcl(String login, String password, String address, String acl) throws SQLException {
        String query = "INSERT INTO accesswebservice(name, login, passwd, host, acl) VALUES (?, ?, ?, ?, ?)";
        Connection c = connFactory.get();
        PreparedStatement st = c.prepareStatement(query);
        st.setString(1, login);
        st.setString(2, login);
        st.setString(3, password);
        st.setString(4, address);
        st.setString(5, acl);
        st.executeUpdate();
        st.close();
    }


    public List<String> getExtensionsInContext(String context) throws SQLException {
        Connection connection = connFactory.get();
        String query = "SELECT exten FROM extensions WHERE context = ?";
        PreparedStatement st = connection.prepareStatement(query);
        st.setString(1, context);
        ResultSet rs = st.executeQuery();
        List<String> result = new LinkedList<>();
        while (rs.next()) {
            result.add(rs.getString("exten"));
        }
        return result;
    }

    public void deleteContext(String context) throws SQLException {
        Connection connection = connFactory.get();
        try {
            connection.setAutoCommit(false);
            deleteContextNumbers(connection, context);
            deleteContextInclusions(connection, context);
            deleteContextOnly(connection, context);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            logger.log(Level.SEVERE, "Could not create the context " + context, e);
            throw e;
        } finally {
            connection.setAutoCommit(true);
        }
    }

    private void deleteContextNumbers(Connection c, String context) throws SQLException {
        String query = "DELETE FROM contextnumbers WHERE context = ?";
        PreparedStatement st = c.prepareStatement(query);
        st.setString(1, context);
        st.executeUpdate();
    }

    private void deleteContextInclusions(Connection c, String context) throws SQLException {
        String query = "DELETE FROM contextinclude WHERE context = ? OR include = ?";
        PreparedStatement st = c.prepareStatement(query);
        st.setString(1, context);
        st.setString(2, context);
        st.executeUpdate();
    }

    private void deleteContextOnly(Connection c, String context) throws SQLException {
        String query = "DELETE FROM context WHERE name = ?";
        PreparedStatement st = c.prepareStatement(query);
        st.setString(1, context);
        st.executeUpdate();
    }

    public void deleteContextInclusion(String context, String includedContext) throws SQLException {
        String query = "DELETE FROM contextinclude WHERE context = ? AND include = ?";
        PreparedStatement st = connFactory.get().prepareStatement(query);
        st.setString(1, context);
        st.setString(2, includedContext);
        st.executeUpdate();
    }

    public boolean contextExists(String context) throws SQLException {
        String query = "SELECT * FROM context WHERE name= ?";
        PreparedStatement statement = connFactory.get().prepareStatement(query);
        statement.setString(1, context);
        ResultSet rows = statement.executeQuery();
        boolean result = rows.next();
        statement.close();
        return result;
    }

    public List<Context> listUserContexts() throws SQLException {
        String query = "SELECT name, contexttype FROM context";
        PreparedStatement statement = connFactory.get().prepareStatement(query);
        ResultSet rows = statement.executeQuery();
        List<Context> contexts = new LinkedList<>();
        while (rows.next()) {
            try {
                contexts.add(getContext(rows.getString("name"), "user"));
            } catch(EmptyContextException e) {
                logger.warning("Not listing empty context " + rows.getString("name"));
            }
        }
        statement.close();
        return contexts;
    }

    public boolean trunkExists(String trunk) throws SQLException {
        String query = "SELECT * FROM usersip WHERE name= ?";
        PreparedStatement statement = connFactory.get().prepareStatement(query);
        statement.setString(1, trunk);
        ResultSet rows = statement.executeQuery();
        boolean result = rows.next();
        statement.close();
        return result;
    }

    public void updateContext(Context context) throws SQLException {
        Connection connection = connFactory.get();
        try {
            connection.setAutoCommit(false);
            updateContextOnly(connection, context);
            deleteContextNumbers(connection, context.getName());
            createContextNumbers(connection, context);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            logger.log(Level.SEVERE, "Could not create the context " + context, e);
            throw e;
        } finally {
            connection.setAutoCommit(true);
        }
    }

    private void updateContextOnly(Connection c, Context context) throws SQLException {
        PreparedStatement st = c.prepareStatement("UPDATE context SET contexttype = ? WHERE name = ?");
        st.setString(1, context.getType().toString());
        st.setString(2, context.getName());
        st.executeUpdate();
    }
}
