package xivo.ldap.xivoconnection;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import xivo.restapi.connection.WebServicesConnector;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.*;

import com.google.inject.Inject;

public class RealXivoConnector implements XivoConnector {

    private WebServicesConnector webServices;
    private PostgresConnector postgres;
    private Logger logger = Logger.getLogger(getClass().getName());

    @Inject
    public RealXivoConnector(WebServicesConnector webServices, PostgresConnector postgres) {
        this.webServices = webServices;
        this.postgres = postgres;
    }

    @Override
    public synchronized List<User> getAllUsers() throws IOException, WebServicesException, SQLException {
        List<User> users = webServices.getAllUsers();
        for (User user : users) {
            user.setIncomingCall(postgres.getIncomingCallForUser(user));
        }
        return users;
    }

    @Override
    public synchronized void updateUser(User user) throws IOException, WebServicesException {
        logger.info("Updating : " + user.toString());
        webServices.updateUser(user);
        if (user.getCtiConfiguration() != null)
            webServices.updateCtiConfiguration(user);
    }

    @Override
    public synchronized IncomingCall getIncomingCallForUser(User user) throws IOException {
        return postgres.getIncomingCallForUser(user);
    }

    @Override
    public synchronized List<String> getFreeSdas() {
        return postgres.getFreeSdas();
    }

    @Override
    public synchronized Info getXivoInfo() throws IOException, WebServicesException {
        return webServices.getInfo();
    }

    @Override
    public synchronized void associateIncallToUser(User user) throws SQLException {
        if (user.getIncomingCall() != null) {
            logger.info("Associating incall for user " + user);
            logger.info("Associating incalls : " + user.toString());
            postgres.associateIncallToUser(user.getIncomingCall(), user.getId());
        }
    }

    @Override
    public synchronized void createLineForUser(User user) throws IOException, WebServicesException {
        if (user.getLine() != null) {
            logger.info("Associating line for user : " + user.toString());
            webServices.createLine(user.getLine());
            webServices.associateLineToUser(user);
        }
    }

    @Override
    public synchronized void createVoicemailForUser(User user) throws IOException, WebServicesException {
        if (user.getVoicemail() != null) {
            logger.info("Creating voicemail for user : " + user.toString());
            webServices.createVoicemail(user.getVoicemail());
            webServices.associateVoicemailToUser(user);
        }
    }

    @Override
    public synchronized void updateLineForUser(User user) throws IOException, WebServicesException {
        Line oldLine = webServices.getLineForUser(user.getId());
        Line newLine = user.getLine();
        if (newLine != null && oldLine != null) {
            logger.info("Updating line for user : " + user.toString());
            newLine.setLineId(oldLine.getLineId());
            newLine.setExtensionId(oldLine.getExtensionId());
            webServices.updateLine(user.getLine());
        }
    }

    @Override
    public synchronized void updateVoicemailForUser(User user) throws IOException, WebServicesException {
        if (user.getVoicemail() != null) {
            logger.info("Updating voicemail for user : " + user.toString());
            webServices.updateVoicemail(user);
            if (webServices.getVoicemailForUser(user.getId()) == null)
                webServices.associateVoicemailToUser(user);
        }
    }

    @Override
    public synchronized void deleteUser(User user) throws IOException, WebServicesException {
        logger.info("Deleting user : " + user.toString());
        if (user.getLine() != null)
            webServices.dissociateLineFromUser(user);
        if (user.getVoicemail() != null)
            webServices.dissociateVoicemailFromUser(user);
        webServices.deleteUser(user);
    }

    @Override
    public synchronized void deleteLineForUser(User user) throws IOException, WebServicesException {
        if (user.getLine() != null) {
            logger.info("Deleting line for user : " + user.toString());
            try {
                webServices.dissociateLineFromUser(user);
            } catch (WebServicesException e) {
                if (e.getStatus() != 404)
                    throw e;
            }
            webServices.deleteLine(user.getLine());
            user.setLine(null);
        }
    }

    @Override
    public synchronized void deleteVoicemailForUser(User user) throws IOException, WebServicesException {
        Voicemail vm = webServices.getVoicemailForUser(user.getId());
        if (vm != null) {
            logger.info("Deleting voicemail  for user : " + user.toString());
            webServices.dissociateVoicemailFromUser(user);
            webServices.deleteVoicemail(vm);
            user.setVoicemail(null);
        }
    }

    @Override
    public synchronized void deleteIncallForUser(User user) throws SQLException {
        IncomingCall userIncomingCall = null;
        try {
            userIncomingCall = this.getIncomingCallForUser(user);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error retrieving the incoming call for user " + user, e);
        }
        if (userIncomingCall == null || userIncomingCall.getSda() == null)
            return;
        logger.info("Deleting incoming call for user : " + user.toString());
        postgres.deleteIncall(userIncomingCall);
    }

    @Override
    public synchronized void updateIncallForUser(User user) throws SQLException, IOException, EmptyContextException,
            NumberOutOfContextException {
        deleteIncallForUser(user);
        createIncallForUser(user);
    }

    @Override
    public synchronized void createIncallForUser(User user) throws EmptyContextException, NumberOutOfContextException,
            SQLException {
        IncomingCall incall = user.getIncomingCall();
        if (incall == null || incall.getSda() == null)
            return;
        validateIncall(incall);
        if (!postgres.incomingCallExists(incall)) {
            logger.info("Creating incoming call for user : " + user.toString());
            postgres.createIncall(incall);
        }
        postgres.associateIncallToUser(incall, user.getId());
    }

    private synchronized void validateIncall(IncomingCall incall) throws EmptyContextException,
            NumberOutOfContextException {
        Context context = postgres.getContext(incall.getContext(), "incall");
        for(ContextInterval it: context.getIntervals()) {
            if(it.isNumberInbound(it.prefix + incall.getSda()))
                return;
        }
        throw new NumberOutOfContextException(incall.getSda(), incall.getContext());
    }

    @Override
    public synchronized CtiProfile getDefaultCtiProfile() throws IOException {
        try {
            return webServices.getCtiProfileByName(DEFAULT_CTI_PROFILE);
        } catch (WebServicesException e) {
            logger.log(Level.SEVERE, "Could not get CTI profile " + DEFAULT_CTI_PROFILE, e);
            return null;
        }
    }

    @Override
    public synchronized void enableLiveReload() throws IOException {
        try {
            webServices.enableLiveReload();
        } catch (WebServicesException e) {
            logger.log(Level.SEVERE, "Could not enable live reload", e);
        }
    }

    @Override
    public synchronized void disableLiveReload() throws IOException {
        try {
            webServices.disableLiveReload();
        } catch (WebServicesException e) {
            logger.log(Level.SEVERE, "Could not enable live reload", e);
        }
    }

    @Override
    public synchronized User getUser(int userId) throws IOException, WebServicesException {
        User user = webServices.getUser(userId);
        user.setIncomingCall(postgres.getIncomingCallForUser(user));
        return user;
    }

    @Override
    public synchronized void createContext(Context context) throws IOException, SQLException {
        postgres.createContext(context);
    }

    @Override
    public synchronized int getUsersNumber() throws IOException, WebServicesException {
        return webServices.getUsersNumber();
    }

    @Override
    public synchronized void createContextInclusion(String context, String includedContext) throws IOException,
            SQLException {
        createContextInclusion(context, includedContext, 0);
    }

    @Override
    public void createContextInclusion(String context, String includedContext, int priority) throws IOException,
            SQLException {
        postgres.createContextInclusion(context, includedContext, priority);
    }

    @Override
    public synchronized void createTrunk(Trunk trunk) throws IOException, SQLException {
        postgres.createTrunk(trunk);
    }

    @Override
    public synchronized User createUser(User user) throws IOException, WebServicesException {
        return webServices.createUser(user);
    }

    @Override
    public synchronized boolean extensionExists(String exten, String context) throws SQLException {
        return postgres.extensionExists(exten, context);
    }

    @Override
    public synchronized void createWebServicesUser(String login, String password, String address) throws SQLException {
        postgres.createWebServicesUser(login, password, address);
    }

    @Override
    public synchronized void createWebServicesUserWithAcl(String login, String password, String address, String acl)
            throws SQLException {
        postgres.createWebServicesUserWithAcl(login, password, address, acl);
    }

    @Override
    public synchronized List<String> getExtensionsInContext(String context) throws SQLException {
        return postgres.getExtensionsInContext(context);
    }

    @Override
    public synchronized void deleteContext(String context) throws SQLException {
        postgres.deleteContext(context);
    }

    @Override
    public synchronized void deleteContextInclusion(String context, String includedContext) throws SQLException {
        postgres.deleteContextInclusion(context, includedContext);
    }

    @Override
    public synchronized boolean contextExists(String context) throws SQLException {
        return postgres.contextExists(context);
    }

    @Override
    public synchronized List<Context> listUserContexts() throws SQLException {
        return postgres.listUserContexts();
    }

    @Override
    public synchronized boolean trunkExists(String trunk) throws SQLException {
        return postgres.trunkExists(trunk);
    }

    @Override
    public synchronized void updateContext(Context context) throws SQLException {
        postgres.updateContext(context);
    }

}
