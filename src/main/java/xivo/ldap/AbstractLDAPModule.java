package xivo.ldap;

import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.impl.client.DefaultHttpClient;

import xivo.ldap.configuration.ConfigLoader;
import xivo.ldap.configuration.Configuration;
import xivo.ldap.ldapconnection.FieldMapping;
import xivo.ldap.ldapconnection.LDAPConnector;
import xivo.ldap.ldapconnection.LDAPConnectorImpl;
import xivo.ldap.ldapconnection.MockLDAPConnector;
import xivo.ldap.xivoconnection.MockXivoConnector;
import xivo.ldap.xivoconnection.RealXivoConnector;
import xivo.ldap.xivoconnection.XivoConnector;
import xivo.restapi.connection.HttpClientFactory;
import xivo.restapi.connection.RestapiConfig;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;

public abstract class AbstractLDAPModule extends AbstractModule {

    private boolean dryRun;

    public AbstractLDAPModule(boolean dryRun) {
        this.dryRun = dryRun;
    }

    @Override
    protected final void configure() {
        bindNames();
        bind(DefaultHttpClient.class).toProvider(HttpClientFactory.class);
        bind(RestapiConfig.class).to(Configuration.class);
        if (dryRun) {
            bind(XivoConnector.class).to(MockXivoConnector.class);
            bind(LDAPConnector.class).to(MockLDAPConnector.class);
        } else {
            bind(XivoConnector.class).to(RealXivoConnector.class);
            bind(LDAPConnector.class).to(LDAPConnectorImpl.class);
        }
        specialBinds();
    }

    /**
     * Cette méthode permet d'associer une interface de xivo-lib-ldap (ou de
     * votre propre projet) à une implémentation spécifique.
     */
    protected abstract void specialBinds();

    @Provides
    public FieldMapping getFieldMapping() {
        return ConfigLoader.getConfig().getFieldMapping();
    }

    @Provides
    public Configuration getConfiguration() {
        return ConfigLoader.getConfig();
    }

    private void bindNames() {
        Configuration config = ConfigLoader.getConfig();
        for (Entry<String, String> entry : config.getParams().entrySet()) {
            bind(String.class).annotatedWith(Names.named(entry.getKey())).toInstance(entry.getValue());
        }
        bind(new TypeLiteral<Set<String>>() {
        }).annotatedWith(Names.named("updatedLdapFields")).toInstance(config.getMasteredFields());
        bind(boolean.class).annotatedWith(Names.named("useTls")).toInstance(config.useTls());
        bind(boolean.class).annotatedWith(Names.named("followReferrals")).toInstance(config.followReferrals());
    }
}
