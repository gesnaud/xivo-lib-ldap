package xivo.ldap.asterisk;

public class CommandException extends Exception {
    private static final long serialVersionUID = -5493152916421182460L;

    public CommandException(String message) {
        super(message);
    }
}
