package xivo.ldap.ldapconnection;

import java.util.List;
import java.util.Set;

import javax.naming.NamingException;

import xivo.restapi.model.User;

public interface LDAPConnector {

    public void updateUsers(List<User> ldapUpdates, Set<String> updatedFields);

    public List<User> getUsers() throws NamingException;

}
