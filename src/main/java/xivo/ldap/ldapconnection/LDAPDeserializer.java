package xivo.ldap.ldapconnection;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;

import xivo.ldap.UserIntrospector;
import xivo.ldap.configuration.ExitCodes;
import xivo.restapi.model.User;

import com.google.inject.Inject;

public class LDAPDeserializer {

    private Logger logger = Logger.getLogger(this.getClass().getName());
    private FieldMapping fieldMapping;
    private UserModifier userModifier;

    @Inject
    public LDAPDeserializer(FieldMapping fieldMapping, UserModifier userModifier) {
        this.fieldMapping = fieldMapping;
        this.userModifier = userModifier;

        logger.info("Field mapping retrieved:");
        for (String field : this.fieldMapping.getLdapFields()) {
            this.logger.info(field + " = " + this.fieldMapping.getXivoFields(field));
        }
    }

    public List<User> createAllUsers(NamingEnumeration<SearchResult> searchResults) {
        List<User> usersList = new LinkedList<User>();
        try {
            while (searchResults.hasMore()) {
                SearchResult result = searchResults.next();
                User user = this.createUser(result);
                if( !usersList.contains(user) ) {
                    usersList.add(user);
                }
                logger.fine("Retrieved user " + user + " from the LDAP");
            }
        } catch (NamingException e) {

            logger.log(Level.SEVERE, "An exception occured while processing the users list, exiting.", e);
            System.exit(ExitCodes.LDAP_ACCESS_ERROR.ordinal());
        } catch (IllegalAccessException e) {
            logger.log(Level.SEVERE, "Error accessing a user field, exiting.", e);
            System.exit(ExitCodes.UNREADABLE_USERFIELD.ordinal());
        } catch (NoSuchFieldException e) {
            logger.log(Level.SEVERE, "Field mapping not properly configured, exiting.", e);
            System.exit(ExitCodes.CONFIGURATION_ERROR.ordinal());
        }
        return usersList;
    }

    private User createUser(SearchResult result) throws NamingException, IllegalAccessException, NoSuchFieldException {
        Attributes attributes = result.getAttributes();
        User user = new User();
        for (String ldapField : this.fieldMapping.getLdapFields()) {
            Attribute attribute = attributes.get(ldapField);
            if (attribute != null) {
                String attrValue = (String) attribute.get();
                for (String xivoField : fieldMapping.getXivoFields(ldapField)) {
                    UserIntrospector.setUserField(user, xivoField, attrValue);
                }
            }
        }
        user = userModifier.modifyXivoUser(user, result);
        return user;
    }
}
