package xivo.ldap.ldapconnection;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;

import org.apache.commons.io.FileUtils;

import xivo.restapi.model.User;

import com.google.inject.Inject;
import com.google.inject.name.Named;

public class MockLDAPConnector implements LDAPConnector {

    public static final String MOCK_LDAP_UPDATE_FILE = "mock_ldap_update.log";

    private String reportFolder;
    private LDAPConnectorImpl realConnector;
    private UserModifier userModifier;
    private Logger logger;
    private UpdateRequestFactory updateRequestFactory;

    @Inject
    public MockLDAPConnector(LDAPConnectorImpl realConnector, UserModifier userModifier, @Named("reportFolder") String reportFolder,
            UpdateRequestFactory factory) {
        this.realConnector = realConnector;
        this.reportFolder = reportFolder;
        this.userModifier = userModifier;
        updateRequestFactory = factory;
        logger = Logger.getLogger(this.getClass().getName());
        File reportDirectory = new File(reportFolder);
        if (!reportDirectory.exists()) {
            reportDirectory.mkdir();
        }
    }

    @Override
    public List<User> getUsers() throws NamingException {
        return realConnector.getUsers();
    }

    @Override
    public void updateUsers(List<User> ldapUpdates, Set<String> updatedFields) {
        logger.info("Writing updates to LDAP to " + reportFolder + MOCK_LDAP_UPDATE_FILE);
        List<String> lines = new LinkedList<String>();
        for (User user : ldapUpdates) {
            User updatedUser = userModifier.modifyLdapUser(user);
            String line;
            try {
                line = getLogLine(updatedUser, updatedFields);
            } catch (NoSuchFieldException e) {
                logger.log(Level.SEVERE, "One of the update fields does not exists, cannot execute the update.", e);
                return;
            } catch (IllegalAccessException e) {
                logger.log(Level.SEVERE, "Cannot access one of the update fields, cannot execute the update.", e);
                return;
            }
            lines.add(line);
        }
        try {
            FileUtils.writeLines(new File(reportFolder + MOCK_LDAP_UPDATE_FILE), lines);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error logging the LDAP updates.", e);
        }
    }

    private String getLogLine(User user, Set<String> updatedFields) throws NoSuchFieldException, IllegalAccessException {
        UpdateRequest updateRequest = updateRequestFactory.createUpdateRequest(user, updatedFields);
        String line = updateRequest.searchCriteria;
        line += " : ";
        for (Entry<String, String> entry : updateRequest.getLdapFields()) {
            line += entry.getKey() + "=" + entry.getValue() + ", ";
        }
        line = line.substring(0, line.length() - 2);
        return line;
    }
}
