package xivo.ldap.ldapconnection;

import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.StartTlsRequest;
import javax.naming.ldap.StartTlsResponse;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import xivo.ldap.configuration.ExitCodes;

public class ContextFactory {

    private String login;
    private String password;
    private String url;
    private boolean useTls;
    private boolean followReferrals;
    private Logger logger;

    @Inject
    public ContextFactory(@Named("ldapLogin") String login, @Named("ldapPassword") String password,
            @Named("ldapUrl") String url, @Named("useTls") boolean useTls, @Named("followReferrals") boolean followReferrals) {
        this.login = login;
        this.password = password;
        this.url = url;
        this.useTls = useTls;
        this.followReferrals = followReferrals;
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    public LdapContext get() {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, this.url);
        if(followReferrals)
            env.put(Context.REFERRAL, "follow");
        else
            env.put(Context.REFERRAL, "ignore");
        env.put("java.naming.ldap.attributes.binary", "objectGUID");
        try {
            LdapContext ctx = new InitialLdapContext(env, null);
            if (useTls) {
                StartTlsResponse tls = (StartTlsResponse) ctx.extendedOperation(new StartTlsRequest());
                tls.negotiate();
            }
            ctx.addToEnvironment(Context.SECURITY_AUTHENTICATION, "simple");
            ctx.addToEnvironment(Context.SECURITY_PRINCIPAL, login);
            ctx.addToEnvironment(Context.SECURITY_CREDENTIALS, password);
            return ctx;
        } catch (NamingException e1) {
            logger.log(Level.SEVERE, "Could not initiate the connection to LDAP, exiting.", e1);
            System.exit(ExitCodes.LDAP_ACCESS_ERROR.ordinal());
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Could not initiate the connection to LDAP, exiting.", e);
            System.exit(ExitCodes.LDAP_ACCESS_ERROR.ordinal());
        }
        // useless, but for compiler:
        return null;
    }
}
