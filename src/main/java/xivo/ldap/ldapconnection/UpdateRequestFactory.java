package xivo.ldap.ldapconnection;

import java.util.Set;

import xivo.ldap.UserIntrospector;
import xivo.restapi.model.User;

import com.google.inject.Inject;

public class UpdateRequestFactory {

    @Inject(optional = true)
    protected UserIdentifier userIdentifier;
    private FieldMapping mapping;

    @Inject
    public UpdateRequestFactory(FieldMapping mapping) {
        this.mapping = mapping;
    }

    public UpdateRequest createUpdateRequest(User user, Set<String> updatedLdapFields) throws NoSuchFieldException,
    IllegalAccessException {
        String searchCriteria = userIdentifier.getSearchCriteria(user);
        UpdateRequest request = new UpdateRequest(searchCriteria);
        for (String field : updatedLdapFields) {
            String xivoField = null;
            if (mapping.getXivoFields(field) != null) {
                xivoField = mapping.getXivoFields(field).iterator().next();
            }
            else {
                throw new NoSuchFieldException(field);
            }
            Object fieldValue = UserIntrospector.getUserField(user, xivoField);
            if (fieldValue != null && !fieldValue.toString().equals("")) {
                request.addLdapField(field, fieldValue.toString());
            }
        }
        return request;
    }
}
