package xivo.ldap.ldapconnection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FieldMapping {

    private Map<String, Set<String>> map = new HashMap<String, Set<String>>();

    public String toString() {
        return map.toString();
    }

    public Set<String> getXivoFields(String ldapField) {
        return map.get(ldapField);
    }

    public void addFieldMapping(String ldapField, String xivoField) {
        Set<String> set = map.get(ldapField);
        if (set == null) {
            set = new HashSet<String>();
            map.put(ldapField, set);
        }
        set.add(xivoField);
    }

    public Set<String> getLdapFields() {
        return map.keySet();
    }

    public Set<String> getAllXivoFields() {
        Set<String> res = new HashSet<>();
        for (Set<String> fields : map.values()) {
            res.addAll(fields);
        }
        return res;
    }

    public String getLdapField(String xivoField) {
        for (String ldapField : map.keySet()) {
            if (map.get(ldapField).contains(xivoField)) {
                return ldapField;
            }
        }
        return null;
    }
}
