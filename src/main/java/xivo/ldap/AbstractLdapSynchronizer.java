package xivo.ldap;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.naming.NamingException;

import org.apache.commons.cli.ParseException;

import xivo.ldap.asterisk.AsteriskManager;
import xivo.ldap.configuration.ConfigLoader;
import xivo.ldap.configuration.Configuration;
import xivo.ldap.configuration.ExitCodes;
import xivo.ldap.ldapconnection.LDAPConnector;
import xivo.ldap.xivoconnection.EmptyContextException;
import xivo.ldap.xivoconnection.NumberOutOfContextException;
import xivo.ldap.xivoconnection.XivoConnector;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.CtiConfiguration;
import xivo.restapi.model.CtiProfile;
import xivo.restapi.model.User;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

public abstract class AbstractLdapSynchronizer {

    protected Configuration config = ConfigLoader.getConfig();
    @Inject
    protected AbstractDeltaGenerator deltaGenerator;
    @Inject
    protected XivoConnector xivoConnector;
    @Inject
    protected LDAPConnector ldapConnector;
    @Inject
    protected Initializer initializer;
    @Inject
    protected AsteriskManager asteriskManager;
    protected List<User> xivoUsers;
    protected List<User> ldapUsers;
    protected static Logger logger;

    public static void start(Class<? extends AbstractLDAPModule> moduleClass, String[] args) {
        initiateLogger();
        ParsingResult parsing = null;
        try {
            parsing = new ArgumentParser().parse(args);
        } catch (ParseException e) {
            logger.log(Level.SEVERE, "Error parsing the parameters : " + Arrays.toString(args), e);
            System.exit(ExitCodes.WRONG_CLI_PARAMETERS.ordinal());
        }
        AbstractLDAPModule module = getModuleInstance(moduleClass, parsing);
        Injector injector = Guice.createInjector(module);
        AbstractLdapSynchronizer synchronizer = injector.getInstance(AbstractLdapSynchronizer.class);
        synchronizer.setUp();
        try {
            synchronizer.retrieveUsers();
        } catch (WebServicesException e) {
            logger.log(Level.SEVERE, "Error with restapi when retrieving the users", e);
            System.exit(ExitCodes.REST_API_ERROR.ordinal());
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error with postgres when retrieving the users", e);
            System.exit(ExitCodes.REST_API_ERROR.ordinal());
        } catch (NamingException e) {
            logger.log(Level.SEVERE, "Error with the LDAP when retrieving the users", e);
            System.exit(ExitCodes.REST_API_ERROR.ordinal());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error when retrieving the users", e);
            System.exit(ExitCodes.REST_API_ERROR.ordinal());
        }
        try {
            if (parsing.isInitiatialization())
                synchronizer.initialize();
            else
                synchronizer.synchronize();
        } catch (WebServicesException e) {
            logger.log(Level.SEVERE, "Error with restapi when performing the synchronization", e);
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error with postgres when performing the synchronization", e);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error when performing the synchronization", e);
        }
    }

    private static void initiateLogger() {
        try {
            FileInputStream configFile = new FileInputStream("config/logging.properties");
            LogManager.getLogManager().readConfiguration(configFile);
            configFile.close();
        } catch (FileNotFoundException e) {
            System.err.println("Could not find the logging configuration file, exiting.");
            System.exit(ExitCodes.LOG_CONFIGURATION_ERROR.ordinal());
        } catch (IOException e) {
            System.err.println("Could not read the logging configuration file, exiting.");
            System.exit(ExitCodes.LOG_CONFIGURATION_ERROR.ordinal());
        }
        logger = Logger.getLogger(AbstractLdapSynchronizer.class.getName());
    }

    private static AbstractLDAPModule getModuleInstance(Class<? extends AbstractLDAPModule> moduleClass,
            ParsingResult parsing) {
        AbstractLDAPModule module = null;
        try {
            module = moduleClass.getConstructor(boolean.class).newInstance(parsing.isDryRun());
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error instantiating " + moduleClass.getName()
                    + ", did you define a constructor with a single boolean parameter ?", e);
            System.exit(ExitCodes.CODE_STRUCTURE_ERROR.ordinal());
        }
        return module;
    }

    protected abstract void setUp();

    protected abstract void performSpecificActions();

    private void retrieveUsers() throws IOException, WebServicesException, SQLException, NamingException {
        xivoUsers = xivoConnector.getAllUsers();
        ldapUsers = ldapConnector.getUsers();
    }

    protected void initialize() throws IOException, WebServicesException, SQLException {
        initializer.initialize(xivoUsers, ldapUsers);
        xivoConnector.disableLiveReload();
        for (User u : initializer.getUsersToUpdate())
            xivoConnector.updateUser(u);
        xivoConnector.enableLiveReload();
    }

    protected void synchronize() throws IOException, WebServicesException, SQLException {
        deltaGenerator.generateDelta(xivoUsers, ldapUsers);
        xivoConnector.disableLiveReload();
        performCreations();
        performUpdates();
        performDeletions();
        performLdapUpdates();
        performSpecificActions();
        xivoConnector.enableLiveReload();
        asteriskManager.reloadCore();
    }

    protected void performLdapUpdates() {
        List<User> ldapUpdates = deltaGenerator.getUpdatesToLdap();
        if (config.updateLdap()) {
            ldapConnector.updateUsers(ldapUpdates, config.getUpdatedLdapFields());
        }
    }

    protected void performDeletions() throws IOException {
        for (User u : deltaGenerator.getUsersToDelete()) {
            try {
                if (config.deleteVoicemails())
                    xivoConnector.deleteVoicemailForUser(u);
                if (config.deleteLines())
                    xivoConnector.deleteLineForUser(u);
                if (config.deleteIncalls())
                    xivoConnector.deleteIncallForUser(u);
                if (config.deleteUsers())
                    xivoConnector.deleteUser(u);
            } catch (WebServicesException | SQLException e) {
                logger.log(Level.SEVERE, "Error deleting the user " + u, e);
            }
        }
        for(User u: deltaGenerator.getUsersToUpdate()) {
            try {
                User original = xivoConnector.getUser(u.getId());
                if(config.deleteLines() && u.getLine() == null && original.getLine() != null)
                    xivoConnector.deleteLineForUser(original);
                if(config.deleteVoicemails() && u.getVoicemail() == null && original.getVoicemail() != null)
                    xivoConnector.deleteVoicemailForUser(original);
                if(config.deleteIncalls() && u.getIncomingCall() == null && original.getIncomingCall() != null)
                    xivoConnector.deleteIncallForUser(original);
            } catch (WebServicesException | SQLException e) {
                logger.log(Level.SEVERE, "Error deleting a subobject for user " + u, e);
            }
        }
    }

    protected void performUpdates() throws IOException {
        for (User u : deltaGenerator.getUsersToUpdate()) {
            try {
                if (config.updateUsers()) {
                    xivoConnector.updateUser(u);
                }
                if (config.updateLines()) {
                    xivoConnector.updateLineForUser(u);
                }
                if (config.updateVoicemails()) {
                    xivoConnector.updateVoicemailForUser(u);
                }
                if (config.updateIncalls()) {
                    xivoConnector.updateIncallForUser(u);
                }
            } catch (WebServicesException | SQLException | EmptyContextException | NumberOutOfContextException e) {
                logger.log(Level.SEVERE, "Error deleting the user " + u, e);
            }
        }

    }

    protected void performCreations() throws IOException {
        List<User> usersToCreate = deltaGenerator.getUsersToCreate();
        List<User> usersToUpdate = deltaGenerator.getUsersToUpdate();
        setDefaultCtiConfig(usersToCreate);
        for (User u : usersToCreate) {
            try {
                if (config.createUsers())
                    xivoConnector.createUser(u);
                if (config.createLines())
                    xivoConnector.createLineForUser(u);
                if (config.createVoicemails())
                    xivoConnector.createVoicemailForUser(u);
                if (config.createIncalls())
                    xivoConnector.createIncallForUser(u);
            } catch (WebServicesException | EmptyContextException | NumberOutOfContextException | SQLException e) {
                logger.log(Level.SEVERE, "Error creating the user " + u, e);
            }
        }

        for (User user : usersToUpdate) {
            try {
                if (config.createLines() && user.getLine() != null && user.getLine().getLineId() == null)
                    xivoConnector.createLineForUser(user);
                if (config.createVoicemails() && user.getVoicemail() != null && user.getVoicemail().getId() == null)
                    xivoConnector.createVoicemailForUser(user);
            } catch (WebServicesException e) {
                logger.log(Level.SEVERE, "Error updating the user " + user, e);
            }
        }
    }

    private void setDefaultCtiConfig(List<User> usersToCreate) throws IOException {
        CtiProfile defaultProfile = xivoConnector.getDefaultCtiProfile();
        CtiConfiguration defaultCtiConfig = new CtiConfiguration(true, defaultProfile);
        for (User user : usersToCreate) {
            if (user.getUsername() != null && user.getPassword() != null) {
                if (user.getCtiConfiguration() == null)
                    user.setCtiConfiguration(defaultCtiConfig);
                else if (user.getCtiConfiguration().getCtiProfile() == null)
                    user.getCtiConfiguration().setCtiProfile(defaultProfile);
            }
        }
    }
}
