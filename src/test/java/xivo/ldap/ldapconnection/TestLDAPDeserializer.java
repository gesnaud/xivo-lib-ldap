package xivo.ldap.ldapconnection;

import java.util.List;
import java.util.logging.Logger;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;

import junit.framework.TestCase;

import org.easymock.EasyMock;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import xivo.ldap.configuration.ExitCodes;
import xivo.restapi.model.User;

@RunWith(PowerMockRunner.class)
@PrepareForTest(LDAPDeserializer.class)
public class TestLDAPDeserializer extends TestCase {

    private class MockUserModifier implements UserModifier {

        private boolean called = false;

        @Override
        public User modifyXivoUser(User user, SearchResult searchResult) throws NamingException {
            called = true;
            return user;
        }

        @Override
        public User modifyLdapUser(User user) {
            assertFalse("Should not happen", true);
            return user;
        }

    }

    @Test
    public void testCreateAllUsers() throws Exception {
        LDAPDeserializer ldapDeserializer = PowerMock.createNicePartialMockForAllMethodsExcept(LDAPDeserializer.class,
                "createAllUsers", NamingEnumeration.class);
        Whitebox.setInternalState(ldapDeserializer, "logger", Logger.getLogger("test"));
        User user1 = new User();
        user1.setFirstname("first");
        User user2 = new User();
        user2.setFirstname("second");
        User userDuplicate = user2;
        SearchResult result1 = PowerMock.createNiceMock(SearchResult.class);
        SearchResult result2 = PowerMock.createNiceMock(SearchResult.class);
        @SuppressWarnings("unchecked")
        NamingEnumeration<SearchResult> results = PowerMock.createNiceMock(NamingEnumeration.class);
        EasyMock.expect(results.hasMore()).andReturn(true);
        EasyMock.expect(results.next()).andReturn(result1);
        PowerMock.expectPrivate(ldapDeserializer, "createUser", result1).andReturn(user1);
        EasyMock.expect(results.hasMore()).andReturn(true);
        EasyMock.expect(results.next()).andReturn(result2);
        PowerMock.expectPrivate(ldapDeserializer, "createUser", result2).andReturn(user2);
        EasyMock.expect(results.hasMore()).andReturn(false);
        PowerMock.replayAll();

        List<User> usersList = ldapDeserializer.createAllUsers(results);

        PowerMock.verifyAll();
        assertEquals(usersList.get(0), user1);
        assertEquals(usersList.get(1), user2);
        assertEquals(usersList.size(), 2);
    }

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();
    @Test
    public void testNamingExceptionWhenCreateAllUsers() throws Exception {
        LDAPDeserializer ldapDeserializer = PowerMock.createNicePartialMockForAllMethodsExcept(LDAPDeserializer.class,
                "createAllUsers", NamingEnumeration.class);
        Whitebox.setInternalState(ldapDeserializer, "logger", Logger.getLogger("test"));
        NamingEnumeration<SearchResult> results = PowerMock.createNiceMock(NamingEnumeration.class);
        EasyMock.expect(results.hasMore()).andReturn(true);
        EasyMock.expect(results.next()).andThrow(new NamingException());
        PowerMock.replayAll();
        exit.expectSystemExitWithStatus(ExitCodes.LDAP_ACCESS_ERROR.ordinal());
        ldapDeserializer.createAllUsers(results);
    }

    @Test
    public void testCreateUser() throws Exception {
        FieldMapping fieldMapping = new FieldMapping();
        fieldMapping.addFieldMapping("sn", "User.lastname");
        fieldMapping.addFieldMapping("givenName", "User.firstname");
        fieldMapping.addFieldMapping("mobile", "User.mobilephonenumber");
        fieldMapping.addFieldMapping("telephoneNumber", "Line.number");
        fieldMapping.addFieldMapping("telephoneNumber", "Voicemail.number");
        fieldMapping.addFieldMapping("sda", "IncomingCall.sda");
        MockUserModifier userModifier = new MockUserModifier();

        LDAPDeserializer ldapDeserializer = new LDAPDeserializer(fieldMapping, userModifier);
        SearchResult result = PowerMock.createNiceMock(SearchResult.class);
        Attributes attributes = PowerMock.createNiceMock(Attributes.class);
        EasyMock.expect(result.getAttributes()).andReturn(attributes);
        Attribute sn = PowerMock.createNiceMock(Attribute.class);
        EasyMock.expect(attributes.get("sn")).andReturn(sn);
        EasyMock.expect(sn.get()).andReturn("the last name");
        Attribute givenName = PowerMock.createNiceMock(Attribute.class);
        EasyMock.expect(attributes.get("givenName")).andReturn(givenName);
        EasyMock.expect(givenName.get()).andReturn("the first name");
        EasyMock.expect(attributes.get("mobile")).andReturn(null);
        Attribute telephoneNumber = PowerMock.createNiceMock(Attribute.class);
        EasyMock.expect(attributes.get("telephoneNumber")).andReturn(telephoneNumber);
        EasyMock.expect(telephoneNumber.get()).andReturn("1234");
        Attribute sda = PowerMock.createNiceMock(Attribute.class);
        EasyMock.expect(attributes.get("sda")).andReturn(sda);
        EasyMock.expect(sda.get()).andReturn("0231312547");
        PowerMock.expectLastCall();
        PowerMock.replayAll();

        User user = Whitebox.invokeMethod(ldapDeserializer, "createUser", result);
        assertEquals(user.getFirstname(), "the first name");
        assertEquals(user.getLastname(), "the last name");
        assertNotNull(user.getLine());
        assertEquals("1234", user.getLine().getNumber());
        assertNotNull(user.getIncomingCall());
        assertEquals("0231312547", user.getIncomingCall().getSda());
        assertNotNull(user.getVoicemail());
        assertEquals("1234", user.getVoicemail().getNumber());
        PowerMock.verifyAll();
        assertTrue(userModifier.called);
    }

    @Test(expected = NoSuchFieldException.class)
    public void testInvalidFieldMapping() throws Exception {
        FieldMapping fieldMapping = new FieldMapping();
        fieldMapping.addFieldMapping("sn", "User.lastname");
        fieldMapping.addFieldMapping("givenName", "firstname");
        UserModifier userModifier = PowerMock.createNiceMock(UserModifier.class);

        LDAPDeserializer ldapDeserializer = new LDAPDeserializer(fieldMapping, userModifier);
        SearchResult result = PowerMock.createNiceMock(SearchResult.class);
        Attributes attributes = PowerMock.createNiceMock(Attributes.class);
        EasyMock.expect(result.getAttributes()).andReturn(attributes);
        Attribute sn = PowerMock.createNiceMock(Attribute.class);
        EasyMock.expect(attributes.get("sn")).andReturn(sn);
        EasyMock.expect(sn.get()).andReturn("the last name");
        Attribute givenName = PowerMock.createNiceMock(Attribute.class);
        EasyMock.expect(attributes.get("givenName")).andReturn(givenName);
        EasyMock.expect(givenName.get()).andReturn("the first name");
        PowerMock.replayAll();

        Whitebox.invokeMethod(ldapDeserializer, "createUser", result);
    }

}
