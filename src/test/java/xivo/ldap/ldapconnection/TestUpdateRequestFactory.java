package xivo.ldap.ldapconnection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.Test;
import org.powermock.api.easymock.PowerMock;

import xivo.restapi.model.IncomingCall;
import xivo.restapi.model.Line;
import xivo.restapi.model.User;
import xivo.restapi.model.Voicemail;

public class TestUpdateRequestFactory {

    private FieldMapping mapping;
    private UserIdentifier userIdentifier;
    private UpdateRequestFactory factory;

    @Test
    public void testCreateUpdateRequest() throws NoSuchFieldException, IllegalAccessException {
        mapping = new FieldMapping();
        mapping.addFieldMapping("sdaPhoneNumber", "IncomingCall.sda");
        mapping.addFieldMapping("lineNumber", "Line.number");
        mapping.addFieldMapping("cn", "User.lastname");
        mapping.addFieldMapping("lineNumber", "Voicemail.number");
        userIdentifier = PowerMock.createMock(UserIdentifier.class);
        factory = new UpdateRequestFactory(mapping);
        factory.userIdentifier = userIdentifier;
        User user = new User();
        user.setFirstname("Pierre");
        user.setLastname("");
        user.setLine(new Line("123"));
        user.setIncomingCall(new IncomingCall("0123456789"));
        Voicemail voicemail = new Voicemail();
        voicemail.setNumber("456");
        user.setVoicemail(voicemail);
        EasyMock.expect(userIdentifier.getSearchCriteria(user)).andReturn("(criteria1)");
        PowerMock.replayAll();

        Set<String> updatedFields = new HashSet<String>();
        updatedFields.add("sdaPhoneNumber");
        updatedFields.add("lineNumber");
        UpdateRequest request = factory.createUpdateRequest(user, updatedFields);

        assertEquals("(criteria1)", request.searchCriteria);
        Set<Entry<String, String>> fields = request.getLdapFields();
        assertSetContains(fields, "sdaPhoneNumber", "0123456789");
        assertSetContains(fields, "lineNumber", "456");
        assertEquals(2, fields.size());
    }

    private void assertSetContains(Set<Entry<String, String>> fields, String key, String value) {
        for (Entry<String, String> e : fields) {
            if (e.getKey().equals(key)) {
                assertEquals(value, e.getValue());
                return;
            }
        }
        fail("Entry " + key + " not found");
    }

    @Test(expected = NoSuchFieldException.class)
    public void testCreateUpdateRequestFail() throws NoSuchFieldException, IllegalAccessException {
        mapping = new FieldMapping();
        mapping.addFieldMapping("sdaPhoneNumber", "IncomingCall.sda");
        userIdentifier = PowerMock.createMock(UserIdentifier.class);
        factory = new UpdateRequestFactory(mapping);
        factory.userIdentifier = userIdentifier;
        User user = new User();
        user.setFirstname("Pierre");
        user.setLastname("");
        user.setLine(new Line("123"));
        user.setIncomingCall(new IncomingCall("0123456789"));
        Voicemail voicemail = new Voicemail();
        voicemail.setNumber("456");
        user.setVoicemail(voicemail);
        EasyMock.expect(userIdentifier.getSearchCriteria(user)).andReturn("(criteria1)");
        PowerMock.replayAll();

        Set<String> updatedFields = new HashSet<String>();
        updatedFields.add("sdaPhoneNumber");
        updatedFields.add("lineNumber");
        factory.createUpdateRequest(user, updatedFields);
    }

}
