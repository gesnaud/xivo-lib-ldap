package xivo.ldap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.powermock.reflect.Whitebox;

import xivo.restapi.model.IncomingCall;
import xivo.restapi.model.Line;
import xivo.restapi.model.User;
import xivo.restapi.model.Voicemail;

public class TestUserIntrospector {

    @Test(expected = ClassNotFoundException.class)
    public void testGetObjectFromUserException() throws Exception {
        User user = new User();
        String objectClass = "unknown";
        Whitebox.invokeMethod(UserIntrospector.class, "getObjectFromUser", user, objectClass);
    }

    @Test
    public void testGetObjectFromUser() throws Exception {
        User user = new User();
        String objectClass = "User";
        Object result = Whitebox.invokeMethod(UserIntrospector.class, "getObjectFromUser", user, objectClass);
        assertEquals(User.class, result.getClass());
        assertEquals(user, result);

        objectClass = "Voicemail";
        result = Whitebox.invokeMethod(UserIntrospector.class, "getObjectFromUser", user, objectClass);
        assertEquals(null, result);

        Line line = new Line("1234");
        user.setLine(line);
        objectClass = "Line";
        result = Whitebox.invokeMethod(UserIntrospector.class, "getObjectFromUser", user, objectClass);
        assertEquals(Line.class, result.getClass());
        assertEquals(line, result);

        objectClass = "IncomingCall";
        result = Whitebox.invokeMethod(UserIntrospector.class, "getObjectFromUser", user, objectClass);
        assertEquals(null, result);
    }

    @Test
    public void testGetOrCreateObjectWithUser() throws Exception {
        User user = new User();
        String objectClass = "User";
        Object result = Whitebox.invokeMethod(UserIntrospector.class, "getOrCreateObjectWithUser", user, objectClass);
        assertEquals(User.class, result.getClass());
        assertEquals(user, result);

        objectClass = "Voicemail";
        result = Whitebox.invokeMethod(UserIntrospector.class, "getOrCreateObjectWithUser", user, objectClass);
        assertEquals(Voicemail.class, result.getClass());
        assertNotNull(user.getVoicemail());

        objectClass = "Line";
        result = Whitebox.invokeMethod(UserIntrospector.class, "getOrCreateObjectWithUser", user, objectClass);
        assertEquals(Line.class, result.getClass());
        assertNotNull(user.getLine());

        objectClass = "IncomingCall";
        result = Whitebox.invokeMethod(UserIntrospector.class, "getOrCreateObjectWithUser", user, objectClass);
        assertEquals(IncomingCall.class, result.getClass());
        assertNotNull(user.getIncomingCall());
    }

    @Test
    public void testGetFirstLevelUserField() throws IllegalArgumentException, IllegalAccessException,
            NoSuchFieldException {
        User user = new User();
        String mobile = "0621005497";
        user.setMobilephonenumber(mobile);

        Object result = UserIntrospector.getUserField(user, "User.mobile_phone_number");

        assertEquals(mobile, result);
    }

    @Test
    public void testGetSecondLevelUserField() throws IllegalArgumentException, IllegalAccessException,
            NoSuchFieldException {
        User user = new User();
        String number = "1000";
        user.setLine(new Line(number));

        Object result = UserIntrospector.getUserField(user, "Line.number");

        assertEquals(number, result);
    }

    @Test
    public void testSetFirstLevelUserField() throws IllegalArgumentException, IllegalAccessException,
            NoSuchFieldException {
        User user = new User();
        String mobile = "0689417523";

        UserIntrospector.setUserField(user, "User.mobile_phone_number", mobile);

        assertEquals(mobile, user.getMobilephonenumber());
    }

    @Test
    public void testSetSecondLevelUserField() throws IllegalArgumentException, IllegalAccessException,
            NoSuchFieldException {
        User user = new User();
        String number = "1000";

        UserIntrospector.setUserField(user, "Line.number", number);

        assertEquals(number, user.getLine().getNumber());
    }

    /**
     * attributes are retrieved as String from the LDAP but may be represented
     * as Integer in the model
     * 
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    @Test
    public void testSetNonStringUserField() throws IllegalArgumentException, IllegalAccessException,
            NoSuchFieldException {
        User user = new User();
        String simult_calls = "4";

        UserIntrospector.setUserField(user, "User.simult_calls", simult_calls);

        assertEquals(user.getSimultCalls().intValue(), 4);
    }

    @Test
    public void testGetSuperclassUserField() throws IllegalArgumentException, IllegalAccessException,
            NoSuchFieldException {
        class LocalUser extends User {
        }

        LocalUser user = new LocalUser();
        user.setId(14);

        assertEquals(14, UserIntrospector.getUserField(user, "User.id"));

        class VeryLocalUser extends LocalUser {
        }

        VeryLocalUser user2 = new VeryLocalUser();
        user2.setId(87);

        assertEquals(87, UserIntrospector.getUserField(user2, "User.id"));
    }

    @Test(expected = NoSuchFieldException.class)
    public void testGetUnexistingUserfieldFails() throws IllegalArgumentException, IllegalAccessException,
            NoSuchFieldException {
        User user = new User();
        UserIntrospector.getUserField(user, "User.trucmuch");
    }

    @Test
    public void testSetSuperclassUserfield() throws IllegalArgumentException, IllegalAccessException,
            NoSuchFieldException {
        class LocalUser extends User {
        }
        LocalUser user = new LocalUser();

        UserIntrospector.setUserField(user, "User.id", "14");

        assertEquals(14, user.getId().intValue());

        class VeryLocalUser extends LocalUser {
        }
        VeryLocalUser user2 = new VeryLocalUser();

        UserIntrospector.setUserField(user2, "User.id", "18");

        assertEquals(18, user2.getId().intValue());
    }

    @Test(expected = NoSuchFieldException.class)
    public void testSetUnexistingUserfieldFails() throws IllegalArgumentException, IllegalAccessException,
            NoSuchFieldException {
        User user = new User();
        UserIntrospector.setUserField(user, "User.trucmuch", "foobar");
    }
}
