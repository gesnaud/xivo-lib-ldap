package xivo.ldap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.commons.cli.ParseException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

public class TestArgumentParser {

    private ArgumentParser parser;
    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setUp() throws Exception {
        parser = new ArgumentParser();
    }

    @Test
    public void testParseDryRun() throws ParseException {
        ParsingResult parsing = parser.parse(new String[] {"--dryrun"});
        assertTrue(parsing.isDryRun());
        assertFalse(parsing.isInitiatialization());
    }

    @Test
    public void testParseInitiate() throws ParseException {
        ParsingResult parsing = parser.parse(new String[] {"--initiate" });
        assertFalse(parsing.isDryRun());
        assertTrue(parsing.isInitiatialization());
    }

    @Test(expected = ParseException.class)
    public void testParseUnknownOption() throws ParseException {
        parser.parse(new String[] {"--dryrun", "--unknownoption" });
    }

    @Test
    public void testParseHelpLong() throws ParseException {
        exit.expectSystemExitWithStatus(0);
        parser.parse(new String[] {"--dryrun", "--help" });
    }

    @Test
    public void testParseHelpShort() throws ParseException {
        exit.expectSystemExitWithStatus(0);
        parser.parse(new String[] {"-d", "-h" });
    }

}
