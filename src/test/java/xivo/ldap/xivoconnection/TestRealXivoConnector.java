package xivo.ldap.xivoconnection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.easymock.PowerMock;

import xivo.restapi.connection.WebServicesConnector;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.*;

public class TestRealXivoConnector {

    private RealXivoConnector xivoConnector;
    private WebServicesConnector webServices;
    private PostgresConnector postgres;
    private List<User> users;
    private User user1, user2;
    private Voicemail voicemail1, voicemail2;
    private CtiConfiguration ctiConfig;
    private CtiProfile ctiProfile;
    private ContextInterval it1 = new ContextInterval("0230211000", "0230213000", 4);
    private ContextInterval it2 = new ContextInterval("0230216000", "0230217000", 4);
    private Context fromExtern = new Context("from-extern", Arrays.asList(it1, it2));

    @Before
    public void setUp() {
        webServices = EasyMock.createMock(WebServicesConnector.class);
        postgres = EasyMock.createMock(PostgresConnector.class);
        xivoConnector = new RealXivoConnector(webServices, postgres);
        user1 = new User();
        user1.setId(1);
        user2 = new User();
        user2.setId(2);
        users = new ArrayList<User>(Arrays.asList(new User[] { user1, user2 }));
        voicemail1 = new Voicemail();
        voicemail2 = new Voicemail();
        user1.setVoicemail(voicemail1);
        user2.setVoicemail(voicemail2);
        ctiProfile = new CtiProfile();
        ctiConfig = new CtiConfiguration(true, ctiProfile);
        user1.setCtiConfiguration(ctiConfig);
    }

    @After
    public void tearDown() {
        PowerMock.resetAll();
    }

    @Test
    public void testCreateUsers() throws IOException, WebServicesException, SQLException {
        User createdUser1 = new User();
        user1.setVoicemail(voicemail1);
        EasyMock.expect(webServices.createUser(user1)).andReturn(createdUser1);
        EasyMock.replay(webServices);

        xivoConnector.createUser(user1);

        EasyMock.verify(webServices);
    }

    @Test
    public void testGetAllUsers() throws IOException, WebServicesException, SQLException {
        user2.setLine(new Line("123"));
        IncomingCall incall1 = new IncomingCall("0123456789"), incall2 = new IncomingCall("0213456789");
        EasyMock.expect(webServices.getAllUsers()).andReturn(users);
        EasyMock.expect(postgres.getIncomingCallForUser(user1)).andReturn(incall1);
        EasyMock.expect(postgres.getIncomingCallForUser(user2)).andReturn(incall2);
        EasyMock.replay(webServices);
        EasyMock.replay(postgres);

        List<User> result = xivoConnector.getAllUsers();

        EasyMock.verify(webServices);
        EasyMock.verify(postgres);
        assertSame(users, result);
        assertSame(incall1, user1.getIncomingCall());
        assertSame(incall2, user2.getIncomingCall());
    }

    @Test
    public void testUpdateUsers() throws IOException, WebServicesException {
        webServices.updateUser(user1);
        webServices.updateCtiConfiguration(user1);
        EasyMock.replay(webServices);

        xivoConnector.updateUser(user1);

        EasyMock.verify(webServices);
    }

    @Test
    public void testDeleteUsers() throws IOException, WebServicesException {
        user1.setLine(new Line("1234"));
        webServices.dissociateLineFromUser(user1);
        webServices.dissociateVoicemailFromUser(user1);
        webServices.deleteUser(user1);
        EasyMock.replay(webServices);

        xivoConnector.deleteUser(user1);

        EasyMock.verify(webServices);
    }

    @Test
    public void testGetIncomingCallForUser() throws IOException, WebServicesException {
        User user = new User();
        IncomingCall expectedResult = new IncomingCall("5000");
        EasyMock.expect(postgres.getIncomingCallForUser(user)).andReturn(expectedResult);
        EasyMock.replay(postgres);

        IncomingCall result = xivoConnector.getIncomingCallForUser(user);

        EasyMock.verify(postgres);
        assertSame(expectedResult, result);
    }

    @Test
    public void testGetFreeSdas() {
        List<String> expectedResult = new LinkedList<String>();
        EasyMock.expect(postgres.getFreeSdas()).andReturn(expectedResult);
        EasyMock.replay(postgres);

        List<String> result = xivoConnector.getFreeSdas();

        EasyMock.verify(postgres);
        assertSame(expectedResult, result);
    }

    @Test
    public void testGetXivoInfo() throws IOException, WebServicesException {
        Info info = EasyMock.createMock(Info.class);
        EasyMock.expect(webServices.getInfo()).andReturn(info);
        EasyMock.replay(webServices);

        Info result = xivoConnector.getXivoInfo();
        assertSame(info, result);
        EasyMock.verify(webServices);
    }

    @Test
    public void testAssociateIncallsToUsers() throws SQLException {
        IncomingCall incall1 = new IncomingCall("123");
        user1.setIncomingCall(incall1);
        postgres.associateIncallToUser(incall1, user1.getId());
        EasyMock.replay(postgres);

        xivoConnector.associateIncallToUser(user1);

        EasyMock.verify(postgres);
    }

    @Test
    public void testCreateLinesForUsers() throws IOException, WebServicesException {
        Line line1 = new Line("123");
        user1.setLine(line1);
        webServices.createLine(line1);
        webServices.associateLineToUser(user1);
        EasyMock.replay(webServices);

        xivoConnector.createLineForUser(user1);

        EasyMock.verify(webServices);
    }

    @Test
    public void testCreateVoicemailsForUsers() throws SQLException, IOException, WebServicesException {
        user1.setVoicemail(new Voicemail());
        webServices.createVoicemail(user1.getVoicemail());
        webServices.associateVoicemailToUser(user1);
        EasyMock.replay(webServices);

        xivoConnector.createVoicemailForUser(user1);

        EasyMock.verify(webServices);
    }

    @Test
    public void testUpdateVoicemailsForUsers() throws IOException, WebServicesException {
        webServices.updateVoicemail(user1);
        EasyMock.expect(webServices.getVoicemailForUser(user1.getId())).andReturn(voicemail1);
        webServices.updateVoicemail(user2);
        EasyMock.expect(webServices.getVoicemailForUser(user2.getId())).andReturn(null);
        webServices.associateVoicemailToUser(user2);
        EasyMock.replay(webServices);

        xivoConnector.updateVoicemailForUser(user1);
        xivoConnector.updateVoicemailForUser(user2);

        EasyMock.verify(webServices);
    }

    @Test
    public void testUpdateLineForUser() throws IOException, WebServicesException {
        Line line1 = new Line("1234");
        line1.setLineId(3);
        user1.setLine(line1);
        Line line2 = new Line("1235");
        Line oldLine2 = new Line("1236");
        oldLine2.setLineId(4);
        oldLine2.setExtensionId(5);
        Line line2WithId = new Line("1235");
        line2WithId.setLineId(4);
        line2WithId.setExtensionId(5);
        user2.setLine(line2);
        EasyMock.expect(webServices.getLineForUser(user1.getId())).andReturn(line1);
        webServices.updateLine(line1);
        EasyMock.expect(webServices.getLineForUser(user2.getId())).andReturn(oldLine2);
        webServices.updateLine(line2WithId);
        EasyMock.replay(webServices);

        xivoConnector.updateLineForUser(user1);
        xivoConnector.updateLineForUser(user2);

        EasyMock.verify(webServices);
    }

    @Test
    public void testDeleteLineForUser() throws IOException, WebServicesException {
        Line line1 = new Line("1234");
        user1.setLine(line1);
        webServices.dissociateLineFromUser(user1);
        webServices.deleteLine(line1);
        EasyMock.replay(webServices);

        xivoConnector.deleteLineForUser(user1);

        EasyMock.verify(webServices);
        assertNull(user1.getLine());
    }

    @Test
    public void testCreateIncallsForUsers() throws IOException, WebServicesException, SQLException,
            EmptyContextException, NumberOutOfContextException {

        IncomingCall incall1 = new IncomingCall("2345");
        user1.setIncomingCall(incall1);
        IncomingCall incall2 = new IncomingCall("6789");
        user2.setIncomingCall(incall2);

        EasyMock.expect(postgres.getContext(incall1.getContext(), "incall")).andReturn(fromExtern);
        EasyMock.expect(postgres.incomingCallExists(incall1)).andReturn(true);
        postgres.associateIncallToUser(incall1, user1.getId());
        EasyMock.expect(postgres.getContext(incall2.getContext(), "incall")).andReturn(fromExtern);
        EasyMock.expect(postgres.incomingCallExists(incall2)).andReturn(false);
        postgres.createIncall(incall2);
        postgres.associateIncallToUser(incall2, user2.getId());
        EasyMock.replay(postgres);

        xivoConnector.createIncallForUser(user1);
        xivoConnector.createIncallForUser(user2);

        EasyMock.verify(postgres);
    }

    @Test
    public void testDeleteIncallsForUsers() throws SQLException, IOException {
        IncomingCall userIncomingCall = new IncomingCall("7894");
        EasyMock.expect(postgres.getIncomingCallForUser(user1)).andReturn(userIncomingCall);
        postgres.deleteIncall(userIncomingCall);
        EasyMock.replay(postgres);

        xivoConnector.deleteIncallForUser(user1);

        EasyMock.verify(postgres);
    }

    @Test
    public void testDeleteIncallsForUsersUnabletoGetItFromBase() throws SQLException, IOException {
        EasyMock.expect(postgres.getIncomingCallForUser(user1)).andThrow(new IOException("unable to get"));
        EasyMock.replay(postgres);

        xivoConnector.deleteIncallForUser(user1);

        EasyMock.verify(postgres);
    }

    @Test
    public void testUpdateIncallsForUsers() throws IOException, SQLException, EmptyContextException,
            NumberOutOfContextException {
        IncomingCall incall1 = new IncomingCall("2345");
        user1.setIncomingCall(incall1);
        XivoConnector localConnector = PowerMock.createPartialMock(RealXivoConnector.class, "createIncallForUser",
                "deleteIncallForUser");
        localConnector.deleteIncallForUser(user1);
        localConnector.createIncallForUser(user1);
        EasyMock.replay(localConnector);

        localConnector.updateIncallForUser(user1);

        EasyMock.verify(localConnector);
    }

    @Test
    public void testDeleteVoicemailForUser() throws WebServicesException, IOException {
        EasyMock.expect(webServices.getVoicemailForUser(user1.getId())).andReturn(voicemail1);
        webServices.dissociateVoicemailFromUser(user1);
        webServices.deleteVoicemail(voicemail1);
        EasyMock.replay(webServices);

        xivoConnector.deleteVoicemailForUser(user1);
        EasyMock.verify(webServices);
        assertNull(user1.getVoicemail());
    }

    @Test
    public void testDeleteVoicemailForUser_vmNotSet() throws WebServicesException, IOException {
        user1.setVoicemail(null);
        EasyMock.expect(webServices.getVoicemailForUser(user1.getId())).andReturn(voicemail1);
        webServices.dissociateVoicemailFromUser(user1);
        webServices.deleteVoicemail(voicemail1);
        EasyMock.replay(webServices);

        xivoConnector.deleteVoicemailForUser(user1);
        EasyMock.verify(webServices);
    }

    @Test
    public void testDeleteVoicemailForUser_noVm() throws WebServicesException, IOException {
        user1.setVoicemail(null);
        EasyMock.expect(webServices.getVoicemailForUser(user1.getId())).andReturn(null);
        EasyMock.replay(webServices);

        xivoConnector.deleteVoicemailForUser(user1);
        EasyMock.verify(webServices);
    }

    @Test
    public void testGetDefaultCtiProfile() throws IOException, WebServicesException {
        EasyMock.expect(webServices.getCtiProfileByName(XivoConnector.DEFAULT_CTI_PROFILE)).andReturn(ctiProfile);
        EasyMock.replay(webServices);

        CtiProfile result = xivoConnector.getDefaultCtiProfile();

        assertSame(result, ctiProfile);
        EasyMock.verify(webServices);
    }

    @Test
    public void testEnableLiveReload() throws IOException, WebServicesException {
        webServices.enableLiveReload();
        EasyMock.replay(webServices);

        xivoConnector.enableLiveReload();

        EasyMock.verify(webServices);
    }

    @Test
    public void testDisableLiveReload() throws IOException, WebServicesException {
        webServices.disableLiveReload();
        EasyMock.replay(webServices);

        xivoConnector.disableLiveReload();

        EasyMock.verify(webServices);
    }

    @Test
    public void testGetUser() throws IOException, WebServicesException, SQLException {
        int userId = 5;
        IncomingCall incall = new IncomingCall("0123456789");
        EasyMock.expect(webServices.getUser(userId)).andReturn(user1);
        EasyMock.expect(postgres.getIncomingCallForUser(user1)).andReturn(incall);
        EasyMock.replay(webServices);
        EasyMock.replay(postgres);

        User result = xivoConnector.getUser(userId);

        EasyMock.verify(webServices);
        EasyMock.verify(postgres);
        assertSame(user1, result);
        assertSame(incall, user1.getIncomingCall());
    }

    @Test
    public void testCreateContext() throws SQLException, IOException {
        Context context = new Context("foo", Collections.singletonList(new ContextInterval("300", "399")));
        postgres.createContext(context);
        EasyMock.replay(postgres);

        xivoConnector.createContext(context);

        EasyMock.verify(postgres);
    }

    @Test
    public void testGetUsersNumber() throws Exception {
        int usersNum = 24;
        EasyMock.expect(webServices.getUsersNumber()).andReturn(usersNum);
        EasyMock.replay(webServices);

        int res = xivoConnector.getUsersNumber();

        assertEquals(usersNum, res);
        EasyMock.verify(webServices);
    }

    @Test
    public void testCreateContextInclusion() throws SQLException, IOException {
        postgres.createContextInclusion("default", "other", 0);
        EasyMock.replay(postgres);

        xivoConnector.createContextInclusion("default", "other");

        EasyMock.verify(postgres);
    }

    @Test
    public void testCreateContextInclusion_explicitPriority() throws SQLException, IOException {
        postgres.createContextInclusion("default", "other", 5);
        EasyMock.replay(postgres);

        xivoConnector.createContextInclusion("default", "other", 5);

        EasyMock.verify(postgres);
    }

    @Test
    public void testCreateTrunk() throws SQLException, IOException {
        Trunk t = new Trunk("test", "127.0.0.1", "from-extern");
        postgres.createTrunk(t);
        EasyMock.replay(postgres);

        xivoConnector.createTrunk(t);

        EasyMock.verify(postgres);
    }

    @Test
    public void testCreateUser() throws IOException, WebServicesException, SQLException {
        User createdUser = new User();
        EasyMock.expect(webServices.createUser(user1)).andReturn(createdUser);
        EasyMock.replay(webServices);

        User otherUser = xivoConnector.createUser(user1);

        EasyMock.verify(webServices);
        assertSame(createdUser, otherUser);
    }

    @Test
    public void testExtensionExists() throws IOException, SQLException {
        String exten = "2000", context = "test";
        EasyMock.expect(postgres.extensionExists(exten, context)).andReturn(false);
        EasyMock.replay(postgres);

        assertFalse(xivoConnector.extensionExists(exten, context));
        EasyMock.verify(postgres);
    }

    @Test
    public void testCreateWebServicesUser() throws SQLException {
        String login = "login", password = "password", address = "192.168.6.3";
        postgres.createWebServicesUser(login, password, address);
        EasyMock.replay(postgres);

        xivoConnector.createWebServicesUser(login, password, address);
        EasyMock.verify(postgres);
    }

    @Test
    public void testCreateWebServicesUserWithAcl() throws SQLException {
        String login = "login", password = "password", address = "192.168.6.3", acl = "{agentd.#}";
        postgres.createWebServicesUserWithAcl(login, password, address, acl);
        EasyMock.replay(postgres);

        xivoConnector.createWebServicesUserWithAcl(login, password, address, acl);
        EasyMock.verify(postgres);
    }

    @Test
    public void testGetExtensionsInContext() throws SQLException {
        List<String> result = Arrays.asList("2000", "2001", "2005");
        String context = "test";
        EasyMock.expect(postgres.getExtensionsInContext(context)).andReturn(result);
        EasyMock.replay(postgres);

        assertEquals(result, xivoConnector.getExtensionsInContext(context));
        EasyMock.verify(postgres);
    }

    @Test
    public void testDeleteContext() throws SQLException {
        postgres.deleteContext("test");
        EasyMock.replay(postgres);

        xivoConnector.deleteContext("test");

        EasyMock.verify(postgres);
    }

    @Test
    public void testDeleteContextInclusion() throws SQLException {
        postgres.deleteContextInclusion("default", "test");
        EasyMock.replay(postgres);

        xivoConnector.deleteContextInclusion("default", "test");

        EasyMock.verify(postgres);
    }

    @Test
    public void testContextExists() throws SQLException {
        EasyMock.expect(postgres.contextExists("test")).andReturn(false);
        EasyMock.expect(postgres.contextExists("test2")).andReturn(true);
        EasyMock.replay(postgres);

        assertFalse(xivoConnector.contextExists("test"));
        assertTrue(xivoConnector.contextExists("test2"));
        EasyMock.verify(postgres);
    }

    @Test
    public void testListContexts() throws SQLException {
        List<Context> contexts = Collections.<Context> emptyList();
        EasyMock.expect(postgres.listUserContexts()).andReturn(contexts);
        EasyMock.replay(postgres);

        List<Context> result = xivoConnector.listUserContexts();

        assertEquals(contexts, result);
        EasyMock.verify(postgres);
    }

    @Test
    public void testTrunkExists() throws SQLException {
        EasyMock.expect(postgres.trunkExists("to-xivo1")).andReturn(true);
        EasyMock.expect(postgres.trunkExists("to-xivo2")).andReturn(false);
        EasyMock.replay(postgres);

        assertTrue(xivoConnector.trunkExists("to-xivo1"));
        assertFalse(xivoConnector.trunkExists("to-xivo2"));
        EasyMock.verify(postgres);
    }

    @Test
    public void testUpdateContext() throws SQLException {
        Context context = new Context("foo", Collections.singletonList(new ContextInterval("300", "399")));
        postgres.updateContext(context);
        EasyMock.replay(postgres);

        xivoConnector.updateContext(context);

        EasyMock.verify(postgres);
    }
}
