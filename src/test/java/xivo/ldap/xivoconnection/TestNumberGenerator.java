package xivo.ldap.xivoconnection;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertSame;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.*;

import org.easymock.EasyMock;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import xivo.restapi.model.Context;
import xivo.restapi.model.ContextInterval;

public class TestNumberGenerator {
    private Context context = new Context("default", Collections.singletonList(new ContextInterval("52100", "52199", 3)));

    @Test
    public void testGenerateLineNumber() throws EmptyContextException, SQLException {
        List<String> numbers = Arrays.asList("100", "105", "107");
        NumberGenerator generator = new NumberGenerator(context, numbers);

        String generatedNumber1 = generator.generateLineNumber();
        String generatedNumber2 = generator.generateLineNumber();

        assertEquals("52101", generatedNumber1);
        assertEquals("52102", generatedNumber2);
        assertEquals(Arrays.asList("101", "102"), Whitebox.getInternalState(generator, "generatedNumbers"));
    }

    @Test
    public void testGenerateLineNumberNoneAvailable() throws EmptyContextException, SQLException {
        List<String> numbers = Arrays.asList("100", "101", "102");
        Context context = new Context("default", Collections.singletonList(new ContextInterval("100", "102")));
        NumberGenerator generator = new NumberGenerator(context, numbers);

        String generatedNumber = generator.generateLineNumber();

        assertNull(generatedNumber);
    }

    @Test
    public void testGenerateLineNumber_secondInterval() {
        List<String> numbers = Arrays.asList("0100", "0101", "0102");
        ContextInterval it1 = new ContextInterval("0100", "0102"), it2 = new ContextInterval("52000", "52020", 4);
        Context context = new Context("default", Arrays.asList(it1, it2));
        NumberGenerator generator = new NumberGenerator(context, numbers);

        String generatedNumber = generator.generateLineNumber();

        assertEquals("52000", generatedNumber);
        assertEquals(Collections.singletonList("2000"), Whitebox.getInternalState(generator, "generatedNumbers"));
    }

    @Test
    public void testAddExistingNumber() {
        List<String> numbers = new LinkedList<>(Arrays.asList("100", "101", "102"));
        NumberGenerator generator = new NumberGenerator(context, numbers);

        generator.addExistingNumber("105");

        List<String> list = Whitebox.getInternalState(generator, "existingNumbers");
        assertTrue(list.contains("105"));
    }

    @Test
    public void testIsNumberUsed() {
        List<String> existingNumbers = Arrays.asList("100", "101", "102");
        List<String> generatedNumbers = Arrays.asList("103", "104");
        NumberGenerator generator = new NumberGenerator(context, existingNumbers);
        Whitebox.setInternalState(generator, "generatedNumbers", generatedNumbers);

        assertTrue(generator.isNumberUsed("102"));
        assertTrue(generator.isNumberUsed("104"));
        assertFalse(generator.isNumberUsed("105"));
    }

    @Test
    public void testGetIntervalForNumber() {
        Context context = EasyMock.createMock(Context.class);
        ContextInterval it = EasyMock.createMock(ContextInterval.class);
        NumberGenerator generator = new NumberGenerator(context, Collections.<String>emptyList());
        EasyMock.expect(context.getIntervalForNumber("10000")).andReturn(it);
        EasyMock.replay(context);

        assertSame(it, generator.getIntervalForNumber("10000"));
    }
}
