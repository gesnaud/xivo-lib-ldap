package xivo.ldap;

import static org.easymock.EasyMock.anyInt;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import xivo.ldap.asterisk.AsteriskManager;
import xivo.ldap.configuration.Actions;
import xivo.ldap.configuration.ConfigLoader;
import xivo.ldap.configuration.Configuration;
import xivo.ldap.ldapconnection.LDAPConnector;
import xivo.ldap.xivoconnection.EmptyContextException;
import xivo.ldap.xivoconnection.NumberOutOfContextException;
import xivo.ldap.xivoconnection.XivoConnector;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ConfigLoader.class)
public class TestAbstractLdapSynchronizer {

    private class LdapSynchronizer extends AbstractLdapSynchronizer {
        private LdapSynchronizer(Initializer initializer, XivoConnector xivoConnector, LDAPConnector ldapConnector,
                AbstractDeltaGenerator deltaGenerator, AsteriskManager asteriskManager) {
            this.initializer = initializer;
            this.xivoConnector = xivoConnector;
            this.deltaGenerator = deltaGenerator;
            this.ldapConnector = ldapConnector;
            this.asteriskManager = asteriskManager;
        }

        @Override
        protected void setUp() {
        }

        @Override
        protected void performSpecificActions() {
        }
    }

    private Configuration config;
    private Initializer initializer;
    private XivoConnector xivoConnector;
    private LDAPConnector ldapConnector;
    private AbstractDeltaGenerator deltaGenerator;
    private LdapSynchronizer ldapSynchronizer;
    private AsteriskManager asteriskManager;
    private LinkedList<User> xivoUsers;
    private LinkedList<User> ldapUsers;
    private Set<String> ldapFields;

    @Before
    public void setUp() {
        config = new Configuration();
        PowerMock.mockStatic(ConfigLoader.class);
        EasyMock.expect(ConfigLoader.getConfig()).andReturn(config);
        PowerMock.replay(ConfigLoader.class);
        Actions lines = new Actions(true, true, true);
        Actions users = new Actions(true, true, true);
        Actions voicemails = new Actions(true, true, true);
        Actions incalls = new Actions(true, true, true);
        ldapFields = new HashSet<String>();
        ldapFields.add("field01");
        Whitebox.setInternalState(config, "lines", lines);
        Whitebox.setInternalState(config, "users", users);
        Whitebox.setInternalState(config, "voicemails", voicemails);
        Whitebox.setInternalState(config, "incalls", incalls);
        Whitebox.setInternalState(config, "updatedLdapFields", ldapFields);
        initializer = EasyMock.createMock(Initializer.class);
        xivoConnector = EasyMock.createMock(XivoConnector.class);
        ldapConnector = EasyMock.createMock(LDAPConnector.class);
        deltaGenerator = EasyMock.createMock(AbstractDeltaGenerator.class);
        asteriskManager = EasyMock.createMock(AsteriskManager.class);
        ldapSynchronizer = new LdapSynchronizer(initializer, xivoConnector, ldapConnector, deltaGenerator,
                asteriskManager);
        xivoUsers = new LinkedList<User>();
        ldapUsers = new LinkedList<User>();
        Whitebox.setInternalState(ldapSynchronizer, "xivoUsers", xivoUsers);
        Whitebox.setInternalState(ldapSynchronizer, "ldapUsers", ldapUsers);
        ldapSynchronizer.logger = Logger.getLogger("JUnit");
        ldapSynchronizer.logger.setLevel(Level.OFF);
    }

    @Test
    public void testInitialize() throws IOException, WebServicesException, SQLException {
        User user = new User();
        List<User> toUpdate = Arrays.asList(new User[] { user });
        initializer.initialize(xivoUsers, ldapUsers);
        EasyMock.expect(initializer.getUsersToUpdate()).andReturn(toUpdate);
        xivoConnector.disableLiveReload();
        xivoConnector.updateUser(user);
        xivoConnector.enableLiveReload();
        EasyMock.replay(initializer, xivoConnector);

        ldapSynchronizer.initialize();

        EasyMock.verify(initializer, xivoConnector);
    }

    @Test
    public void testSynchronize() throws IOException, WebServicesException, SQLException, EmptyContextException,
            NumberOutOfContextException {
        List<User> toUpdate = new LinkedList<User>(), toCreate = new LinkedList<User>(), toDelete = new LinkedList<User>(), toLdap = new LinkedList<User>();
        User user1 = new User(), user2 = new User(), user3 = new User(), user4 = new User(), user5 = new User(), user6 = new User();
        Line line1 = new Line(), line2 = new Line(), line3 = new Line();
        line1.setLineId(5);
        Voicemail voicemail1 = new Voicemail(), voicemail2 = new Voicemail(), voicemail3 = new Voicemail();
        voicemail1.setName("1");
        voicemail2.setName("2");
        voicemail3.setName("3");
        voicemail1.setId(4);
        user1.setId(1);
        user1.setLine(line1);
        user1.setVoicemail(voicemail1);
        user2.setId(2);
        user2.setLine(line2);
        user2.setVoicemail(voicemail2);
        user3.setId(3);
        user3.setLine(line3);
        user3.setVoicemail(voicemail3);
        toUpdate.add(user1);
        toUpdate.add(user2);
        toUpdate.add(user3);
        user5.setUsername("abcd");
        user5.setPassword("defg");
        toCreate.add(user4);
        toCreate.add(user5);
        toDelete.add(user6);

        deltaGenerator.generateDelta(xivoUsers, ldapUsers);
        xivoConnector.disableLiveReload();
        EasyMock.expect(deltaGenerator.getUpdatesToLdap()).andReturn(toLdap).anyTimes();
        EasyMock.expect(deltaGenerator.getUsersToUpdate()).andReturn(toUpdate).anyTimes();
        EasyMock.expect(deltaGenerator.getUsersToCreate()).andReturn(toCreate).anyTimes();
        EasyMock.expect(deltaGenerator.getUsersToDelete()).andReturn(toDelete).anyTimes();
        ldapConnector.updateUsers(toLdap, ldapFields);
        EasyMock.expect(xivoConnector.getDefaultCtiProfile()).andReturn(new CtiProfile());
        xivoConnector.deleteUser(user6);
        xivoConnector.deleteVoicemailForUser(user6);
        xivoConnector.deleteLineForUser(user6);
        xivoConnector.deleteIncallForUser(user6);
        EasyMock.expect(xivoConnector.getUser(anyInt())).andReturn(new User()).anyTimes();
        xivoConnector.updateUser(user1);
        xivoConnector.updateUser(user2);
        xivoConnector.updateUser(user3);
        xivoConnector.updateLineForUser(user1);
        xivoConnector.updateLineForUser(user2);
        xivoConnector.updateLineForUser(user3);
        xivoConnector.updateVoicemailForUser(user1);
        xivoConnector.updateVoicemailForUser(user2);
        xivoConnector.updateVoicemailForUser(user3);
        xivoConnector.updateIncallForUser(user1);
        xivoConnector.updateIncallForUser(user2);
        xivoConnector.updateIncallForUser(user3);
        EasyMock.expect(xivoConnector.createUser(user4)).andReturn(user4);
        EasyMock.expect(xivoConnector.createUser(user5)).andReturn(user5);
        xivoConnector.createLineForUser(user4);
        xivoConnector.createLineForUser(user5);
        xivoConnector.createVoicemailForUser(user4);
        xivoConnector.createVoicemailForUser(user5);
        xivoConnector.createIncallForUser(user4);
        xivoConnector.createIncallForUser(user5);
        xivoConnector.createLineForUser(user2);
        xivoConnector.createLineForUser(user3);
        EasyMock.expectLastCall().andThrow(new WebServicesException("", 400));
        xivoConnector.createVoicemailForUser(user2);
        xivoConnector.enableLiveReload();
        asteriskManager.reloadCore();
        EasyMock.replay(deltaGenerator, xivoConnector, ldapConnector, asteriskManager);

        ldapSynchronizer.synchronize();

        EasyMock.verify(deltaGenerator, xivoConnector, ldapConnector, asteriskManager);
        assertNull(user4.getCtiConfiguration());
        assertNotNull(user5.getCtiConfiguration());
    }

    @Test
    public void testSynchronize_deleteSubuobjectsIfRequired() throws IOException, WebServicesException, SQLException {
        User u1 = new User(), u2 = new User(), u3 = new User();
        User original1 = new User(), original2 = new User(), original3 = new User();
        u1.setId(1);
        original1.setId(1);
        original1.setLine(new Line());
        u2.setId(2);
        original2.setId(2);
        original2.setVoicemail(new Voicemail());
        u3.setId(3);
        original3.setId(3);
        original3.setIncomingCall(new IncomingCall("0123"));
        List<User> toUpdate = Arrays.asList(u1, u2, u3);

        EasyMock.expect(xivoConnector.getUser(1)).andReturn(original1);
        EasyMock.expect(xivoConnector.getUser(2)).andReturn(original2);
        EasyMock.expect(xivoConnector.getUser(3)).andReturn(original3);
        EasyMock.expect(deltaGenerator.getUsersToUpdate()).andReturn(toUpdate).anyTimes();
        EasyMock.expect(deltaGenerator.getUsersToDelete()).andReturn(new LinkedList<User>()).anyTimes();
        xivoConnector.deleteLineForUser(original1);
        xivoConnector.deleteVoicemailForUser(original2);
        xivoConnector.deleteIncallForUser(original3);
        EasyMock.replay(xivoConnector, deltaGenerator);

        ldapSynchronizer.performDeletions();

        EasyMock.verify(xivoConnector);
    }
}
