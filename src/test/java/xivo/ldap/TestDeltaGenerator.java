package xivo.ldap;

import java.util.*;
import java.util.logging.Logger;

import junit.framework.TestCase;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import xivo.ldap.configuration.ConfigLoader;
import xivo.ldap.configuration.Configuration;
import xivo.ldap.ldapconnection.FieldMapping;
import xivo.restapi.model.IncomingCall;
import xivo.restapi.model.Line;
import xivo.restapi.model.User;
import xivo.restapi.model.Voicemail;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ConfigLoader.class)
public class TestDeltaGenerator extends TestCase {

    private AbstractDeltaGenerator deltaGenerator;

    @Override
    @Before
    public void setUp() throws SecurityException, NoSuchMethodException {
        deltaGenerator = PowerMock.createMock(AbstractDeltaGenerator.class,
                AbstractDeltaGenerator.class.getDeclaredMethod("xivoNeedsUpdate", User.class, User.class),
                AbstractDeltaGenerator.class.getDeclaredMethod("ldapNeedsUpdate", User.class, User.class),
                AbstractDeltaGenerator.class.getDeclaredMethod("needsCreation", User.class),
                AbstractDeltaGenerator.class.getDeclaredMethod("needsDeletion", User.class));
        deltaGenerator.initializedUsers = new LinkedList<>();
        deltaGenerator.updatesToLdap = new LinkedList<>();
        deltaGenerator.usersToCreate = new LinkedList<>();
        deltaGenerator.usersToDelete = new LinkedList<>();
        deltaGenerator.usersToUpdate = new LinkedList<>();
        Set<String> fieldsToCopy = new HashSet<>();
        fieldsToCopy.add("User.firstname");
        fieldsToCopy.add("User.lastname");
        fieldsToCopy.add("User.description");
        fieldsToCopy.add("Voicemail.email");
        deltaGenerator.fieldsToCopy = fieldsToCopy;
        deltaGenerator.logger = Logger.getLogger("Test");
    }

    @Test
    public void testGenerateDelta() {
        User xivo1 = new User(), xivo2 = new User(), xivo3 = new User(), xivo4 = new User(), ldap1 = new User(), ldap2 = new User(), ldap3 = new User(), ldap4 = new User();
        Line xivoLine = new Line(), ldapLine = new Line();
        Voicemail xivoVoicemail = new Voicemail(), ldapVoicemail = new Voicemail();
        xivoVoicemail.setId(4);
        xivo1.setLine(xivoLine);
        xivo1.setVoicemail(xivoVoicemail);
        xivo1.setDescription("desc1");
        xivo1.setId(1);
        ldap1.setLine(ldapLine);
        ldapVoicemail.setEmail("dupond@test.com");
        ldap1.setVoicemail(ldapVoicemail);
        ldap1.setDescription("desc1");
        ldap1.setFirstname("Pierre");
        ldap1.setLastname("Dupond");
        xivo2.setDescription("desc2");
        xivo2.setId(2);
        ldap2.setDescription("desc2");
        xivo3.setDescription("desc3");
        xivo3.setId(3);
        xivo3.setLine(new Line("1234"));
        xivo3.setLastname("Durand");
        ldap3.setDescription("desc3");
        ldap3.setFirstname("Jean");
        xivo4.setDescription("descXivo4");
        xivo4.setId(4);
        ldap4.setDescription("descLdap4");

        List<User> xivoUsers = new LinkedList<>(Arrays.asList(xivo1, xivo2, xivo3, xivo4));
        List<User> ldapUsers = new LinkedList<>(Arrays.asList(ldap1, ldap2, ldap3, ldap4));
        EasyMock.expect(deltaGenerator.ldapNeedsUpdate(xivo1, ldap1)).andReturn(false);
        EasyMock.expect(deltaGenerator.xivoNeedsUpdate(xivo1, ldap1)).andReturn(true);
        EasyMock.expect(deltaGenerator.ldapNeedsUpdate(xivo2, ldap2)).andReturn(true);
        EasyMock.expect(deltaGenerator.xivoNeedsUpdate(xivo2, ldap2)).andReturn(false);
        EasyMock.expect(deltaGenerator.ldapNeedsUpdate(xivo3, ldap3)).andReturn(true);
        EasyMock.expect(deltaGenerator.xivoNeedsUpdate(xivo3, ldap3)).andReturn(true);
        EasyMock.expect(deltaGenerator.needsCreation(ldap4)).andReturn(true);
        EasyMock.expect(deltaGenerator.needsDeletion(xivo4)).andReturn(true);
        PowerMock.replay(deltaGenerator);

        deltaGenerator.generateDelta(xivoUsers, ldapUsers);

        List<User> expectedCreations = Collections.singletonList(ldap4);
        List<User> expectedDeletions = Collections.singletonList(xivo4);
        List<User> expectedUpdates = Arrays.asList(xivo1, xivo3);
        List<User> expectedLdapUpdates = Arrays.asList(xivo2, xivo3);

        assertEquals(expectedCreations, deltaGenerator.getUsersToCreate());
        assertEquals(expectedDeletions, deltaGenerator.getUsersToDelete());
        assertEquals(expectedUpdates, deltaGenerator.getUsersToUpdate());
        assertEquals("Pierre", xivo1.getFirstname());
        assertEquals("Dupond", xivo1.getLastname());
        assertNotNull(xivo1.getVoicemail());
        assertEquals("dupond@test.com", xivo1.getVoicemail().getEmail());
        assertEquals("Jean", xivo3.getFirstname());
        assertNull(xivo3.getLastname());
        assertEquals(expectedLdapUpdates, deltaGenerator.getUpdatesToLdap());
        PowerMock.verify(deltaGenerator);
    }

    @Test
    public void testGenerateDelta_nullDescriptions() {
        User xivo1 = new User(), ldap1 = new User();
        xivo1.setDescription(null);
        ldap1.setDescription(null);
        List<User> xivoUsers = new LinkedList<>(Collections.singletonList(xivo1));
        List<User> ldapUsers = new LinkedList<>(Collections.singletonList(ldap1));
        EasyMock.expect(deltaGenerator.needsCreation(ldap1)).andReturn(true);
        PowerMock.replay(deltaGenerator);

        deltaGenerator.generateDelta(xivoUsers, ldapUsers);

        List<User> expectedUsersToCreate = new LinkedList<>(Collections.singletonList(ldap1));
        assertEquals(expectedUsersToCreate, deltaGenerator.getUsersToCreate());
        assertEquals(0, deltaGenerator.getUsersToDelete().size());
        assertEquals(0, deltaGenerator.getUsersToUpdate().size());
        assertEquals(0, deltaGenerator.getUpdatesToLdap().size());
        PowerMock.verify(deltaGenerator);
    }

    @Test
    public void testConstructor() {
        PowerMock.mockStatic(ConfigLoader.class);
        Configuration config = EasyMock.createMock(Configuration.class);
        EasyMock.expect(ConfigLoader.getConfig()).andReturn(config);
        FieldMapping fm = new FieldMapping();
        fm.addFieldMapping("sn", "User.firstname");
        fm.addFieldMapping("login", "User.username");
        fm.addFieldMapping("login", "User.description");
        fm.addFieldMapping("homePhone", "Line.number");
        EasyMock.expect(config.getFieldMapping()).andReturn(fm);
        Set<String> masteredFields = new HashSet<>();
        masteredFields.add("Line.number");
        EasyMock.expect(config.getMasteredFields()).andReturn(masteredFields);
        PowerMock.replay(ConfigLoader.class, config);

        AbstractDeltaGenerator gen = new AbstractDeltaGenerator() {
            @Override
            protected boolean xivoNeedsUpdate(User xivoUser, User ldapUser) {
                return false;
            }

            @Override
            protected boolean ldapNeedsUpdate(User xivoUser, User ldapUser) {
                return false;
            }
        };

        Set<String> expectedSet = new HashSet<>();
        expectedSet.add("User.firstname");
        expectedSet.add("User.username");
        expectedSet.add("User.description");
        assertEquals(expectedSet, gen.fieldsToCopy);
    }

    @Test
    public void testCopyFields_nullLine_ldapMaster() throws NoSuchFieldException, IllegalAccessException {
        User xivoUser = new User();
        xivoUser.setLine(new Line());
        User ldapUser = new User();
        deltaGenerator.fieldsToCopy = Collections.singleton("Line.name");

        deltaGenerator.copyFieldsFromLdapToXivo(ldapUser, xivoUser);

        assertNull(xivoUser.getLine());
    }

    @Test
    public void testCopyFields_nullLine_xivoMaster() throws NoSuchFieldException, IllegalAccessException {
        User xivoUser = new User();
        xivoUser.setLine(new Line());
        User ldapUser = new User();
        deltaGenerator.fieldsToCopy = Collections.emptySet();

        deltaGenerator.copyFieldsFromLdapToXivo(ldapUser, xivoUser);

        assertNotNull(xivoUser.getLine());
    }

    @Test
    public void testCopyFields_nullIncall_ldapMaster() throws NoSuchFieldException, IllegalAccessException {
        User xivoUser = new User();
        xivoUser.setIncomingCall(new IncomingCall("021233"));
        User ldapUser = new User();
        deltaGenerator.fieldsToCopy = Collections.singleton("IncomingCall.sda");

        deltaGenerator.copyFieldsFromLdapToXivo(ldapUser, xivoUser);

        assertNull(xivoUser.getIncomingCall());
    }

    @Test
    public void testCopyFields_nullIncall_xivoMaster() throws NoSuchFieldException, IllegalAccessException {
        User xivoUser = new User();
        xivoUser.setIncomingCall(new IncomingCall());
        User ldapUser = new User();
        deltaGenerator.fieldsToCopy = Collections.emptySet();

        deltaGenerator.copyFieldsFromLdapToXivo(ldapUser, xivoUser);

        assertNotNull(xivoUser.getIncomingCall());
    }

    @Test
    public void testCopyFields_nullVoicemail_ldapMaster() throws NoSuchFieldException, IllegalAccessException {
        User xivoUser = new User();
        xivoUser.setVoicemail(new Voicemail());
        User ldapUser = new User();
        deltaGenerator.fieldsToCopy = Collections.singleton("Voicemail.name");

        deltaGenerator.copyFieldsFromLdapToXivo(ldapUser, xivoUser);

        assertNull(xivoUser.getVoicemail());
    }

    @Test
    public void testCopyFields_nullVoicemail_xivoMaster() throws NoSuchFieldException, IllegalAccessException {
        User xivoUser = new User();
        xivoUser.setVoicemail(new Voicemail());
        User ldapUser = new User();
        deltaGenerator.fieldsToCopy = Collections.emptySet();

        deltaGenerator.copyFieldsFromLdapToXivo(ldapUser, xivoUser);

        assertNotNull(xivoUser.getVoicemail());
    }
}
