package xivo.ldap.asterisk;

import java.util.logging.Logger;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.easymock.PowerMock;
import org.powermock.reflect.Whitebox;

public class TestAsteriskManager {

    private AsteriskManager asteriskManager;

    @Before
    public void setUp() {
        asteriskManager = PowerMock.createPartialMock(AsteriskManager.class, "executeCommand");
        Logger logger = Logger.getLogger("test");
        Whitebox.setInternalState(asteriskManager, "logger", logger);
    }

    @Test
    public void testReloadVoicemail() throws CommandException {
        asteriskManager.executeCommand(EasyMock.aryEq(new String[] { "/usr/sbin/asterisk", "-rx", "voicemail reload" }));
        EasyMock.replay(asteriskManager);

        asteriskManager.reloadVoicemails();

        EasyMock.verify(asteriskManager);
    }

    @Test
    public void testReloadDialplan() throws CommandException {
        asteriskManager.executeCommand(EasyMock.aryEq(new String[] { "/usr/sbin/asterisk", "-rx", "dialplan reload" }));
        EasyMock.replay(asteriskManager);

        asteriskManager.reloadDialplan();

        EasyMock.verify(asteriskManager);
    }

    @Test
    public void testReloadCore() throws CommandException {
        asteriskManager.executeCommand(EasyMock.aryEq(new String[] { "/usr/sbin/asterisk", "-rx", "core reload" }));
        EasyMock.replay(asteriskManager);

        asteriskManager.reloadCore();

        EasyMock.verify(asteriskManager);

    }

}
